mod parser;
mod processor;
mod science;

fn main() {
    let file_content: String = parser::read_file_input("./input/input.txt");
    let atoms: Vec<science::Atom> = parser::parse_file_content(&file_content);
    let result: science::Cell = processor::process_atoms(&atoms);
    println!(
        "Results: {:?} => {}",
        result,
        result.o2 * result.co2
    )
}
