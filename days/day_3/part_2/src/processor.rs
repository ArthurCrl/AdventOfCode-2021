use crate::science::Atom;
use crate::science::Cell;
use crate::science::Molecule;

pub fn process_atoms(atoms: &Vec<Atom>) -> Cell {
  let mut molecules = Vec::new();
  let number_of_molecules = atoms[0].value.len();
  for _ in 0..number_of_molecules {
    molecules.push(Molecule {
      number_of_one_bits: 0,
      number_of_zero_bits: 0,
    });
  }
  let mut cell = Cell {
    o2: 0,
    co2: 0,
    o2_str: String::from(""),
    co2_str: String::from(""),
  };
  let mut current_atoms = atoms.clone();
  for i in 0..number_of_molecules {
    if current_atoms.len() == 1 {
      break;
    }
    molecules[i].number_of_one_bits = 0;
    molecules[i].number_of_zero_bits = 0;
    for atom in &current_atoms {
      if atom.value[i] == 0 {
        molecules[i].number_of_zero_bits = molecules[i].number_of_zero_bits + 1
      } else {
        molecules[i].number_of_one_bits = molecules[i].number_of_one_bits + 1
      }
    }
    if molecules[i].number_of_one_bits == molecules[i].number_of_zero_bits {
      current_atoms = current_atoms
        .into_iter()
        .filter(|atom| atom.value[i] == 1)
        .collect();
    } else if molecules[i].number_of_one_bits > molecules[i].number_of_zero_bits {
      current_atoms = current_atoms
        .into_iter()
        .filter(|atom| atom.value[i] == 1)
        .collect();
    } else {  
      current_atoms = current_atoms
        .into_iter()
        .filter(|atom| atom.value[i] == 0)
        .collect();
    }
  }
  for val in &current_atoms[0].value[..] {
    cell.o2_str = cell.o2_str + &val.to_string();
  }
  cell.o2 = u64::from_str_radix(cell.o2_str.as_str(), 2).unwrap();

  current_atoms = atoms.clone();

  for i in 0..number_of_molecules {
    if current_atoms.len() == 1 {
      break;
    }
    molecules[i].number_of_one_bits = 0;
    molecules[i].number_of_zero_bits = 0;
    for atom in &current_atoms {
      if atom.value[i] == 0 {
        molecules[i].number_of_zero_bits = molecules[i].number_of_zero_bits + 1
      } else {
        molecules[i].number_of_one_bits = molecules[i].number_of_one_bits + 1
      }
    }
    if molecules[i].number_of_one_bits == molecules[i].number_of_zero_bits {
      current_atoms = current_atoms
        .into_iter()
        .filter(|atom| atom.value[i] == 0)
        .collect();
    } else if molecules[i].number_of_one_bits < molecules[i].number_of_zero_bits {
      current_atoms = current_atoms
        .into_iter()
        .filter(|atom| atom.value[i] == 1)
        .collect();
    } else {
      current_atoms = current_atoms
        .into_iter()
        .filter(|atom| atom.value[i] == 0)
        .collect();
    }
  }
  for val in &current_atoms[0].value[..] {
    cell.co2_str = cell.co2_str + &val.to_string();
  }
  cell.co2 = u64::from_str_radix(cell.co2_str.as_str(), 2).unwrap();
  return cell;
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn validate_atom_processing() {
    let input_data = vec![
      Atom {
        value: vec![0, 0, 1, 0, 0],
      },
      Atom {
        value: vec![1, 1, 1, 1, 0],
      },
      Atom {
        value: vec![1, 0, 1, 1, 0],
      },
      Atom {
        value: vec![1, 0, 1, 1, 1],
      },
      Atom {
        value: vec![1, 0, 1, 0, 1],
      },
      Atom {
        value: vec![0, 1, 1, 1, 1],
      },
      Atom {
        value: vec![0, 0, 1, 1, 1],
      },
      Atom {
        value: vec![1, 1, 1, 0, 0],
      },
      Atom {
        value: vec![1, 0, 0, 0, 0],
      },
      Atom {
        value: vec![1, 1, 0, 0, 1],
      },
      Atom {
        value: vec![0, 0, 0, 1, 0],
      },
      Atom {
        value: vec![0, 1, 0, 1, 0],
      },
    ];
    let expected_data = Cell {
      o2: 23,
      co2: 10,
      o2_str: String::from("10111"),
      co2_str: String::from("01010"),
    };
    assert_eq!(process_atoms(&input_data), expected_data)
  }
}
