#[derive(Debug, PartialEq, Clone)]
pub struct Atom {
  pub value: Vec<u32>,
}

#[derive(Debug, PartialEq)]
pub struct Molecule {
  pub number_of_one_bits: u64,
  pub number_of_zero_bits: u64,
}

#[derive(Debug, PartialEq)]
pub struct Cell {
  pub o2: u64,
  pub co2: u64,
  pub o2_str: String,
  pub co2_str: String,
}
