use crate::science::Atom;
use std::fs;
use std::io::Read;
use std::path::Path;

pub fn read_file_input(path: &str) -> String {
  let mut file = fs::File::open(Path::new(path)).expect("An error occurred while opening the file");
  let mut contents = String::new();
  file
    .read_to_string(&mut contents)
    .expect("An error occurred while reading the file");
  return contents;
}

pub fn parse_file_content(data: &String) -> Vec<Atom> {
  let mut parsed_data = Vec::new();
  for line in data.lines() {
    parsed_data.push(parse_line(line))
  }
  return parsed_data;
}

fn parse_line(line: &str) -> Atom {
  let mut atom = Atom { value: Vec::new() };
  for c in  line.chars() {
    atom.value.push(c.to_digit(2).unwrap())
  }
  return atom
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn should_parse_standard_line_correctly() {
    assert_eq!(parse_line("000000"), Atom { value: vec![0,0,0,0,0,0]});
    assert_eq!(parse_line("1"), Atom { value: vec![1]});
    assert_eq!(parse_line("0101010"), Atom { value: vec![0,1,0,1,0,1,0]});
  }

  #[test]
  fn should_parse_sample_file_correctly() {
    assert_eq!(
      parse_file_content(&read_file_input("./input/sample.txt")),
      vec![
        Atom { value: vec![0,0,1,0,0]},
        Atom { value: vec![1,1,1,1,0]},
        Atom { value: vec![1,0,1,1,0]},
        Atom { value: vec![1,0,1,1,1]},
        Atom { value: vec![1,0,1,0,1]},
        Atom { value: vec![0,1,1,1,1]},
        Atom { value: vec![0,0,1,1,1]},
        Atom { value: vec![1,1,1,0,0]},
        Atom { value: vec![1,0,0,0,0]},
        Atom { value: vec![1,1,0,0,1]},
        Atom { value: vec![0,0,0,1,0]},
        Atom { value: vec![0,1,0,1,0]},
      ]
    );
  }
}
