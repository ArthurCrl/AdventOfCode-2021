use crate::science::Atom;
use crate::science::Cell;
use crate::science::Molecule;

pub fn process_atoms(atoms: Vec<Atom>) -> Cell {
  let mut molecules = Vec::new();
  let number_of_molecules = atoms[0].value.len();
  for _ in 0..number_of_molecules {
    molecules.push(Molecule {
      number_of_one_bits: 0,
      number_of_zero_bits: 0,
    });
  }
  for atom in atoms {
    for i in 0..number_of_molecules {
      if atom.value[i] == 0 {
        molecules[i].number_of_zero_bits = molecules[i].number_of_zero_bits + 1
      } else {
        molecules[i].number_of_one_bits = molecules[i].number_of_one_bits + 1
      }
    }
  }
  let mut cell = Cell {
    gamma: 0,
    epsilon: 0,
    gamma_str: String::from(""),
    epsilon_str: String::from(""),
  };
  for i in 0..number_of_molecules {
    if molecules[i].number_of_one_bits > molecules[i].number_of_zero_bits {
      cell.gamma_str = cell.gamma_str + "1";
      cell.epsilon_str = cell.epsilon_str + "0";
    } else {
      cell.gamma_str = cell.gamma_str + "0";
      cell.epsilon_str = cell.epsilon_str + "1";
    }
  }
  cell.gamma = u64::from_str_radix(cell.gamma_str.as_str(), 2).unwrap();
  cell.epsilon = u64::from_str_radix(cell.epsilon_str.as_str(), 2).unwrap();
  return cell;
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn validate_atom_processing() {
    let input_data = vec![
      Atom {
        value: vec![0, 0, 1, 0, 0],
      },
      Atom {
        value: vec![1, 1, 1, 1, 0],
      },
      Atom {
        value: vec![1, 0, 1, 1, 0],
      },
      Atom {
        value: vec![1, 0, 1, 1, 1],
      },
      Atom {
        value: vec![1, 0, 1, 0, 1],
      },
      Atom {
        value: vec![0, 1, 1, 1, 1],
      },
      Atom {
        value: vec![0, 0, 1, 1, 1],
      },
      Atom {
        value: vec![1, 1, 1, 0, 0],
      },
      Atom {
        value: vec![1, 0, 0, 0, 0],
      },
      Atom {
        value: vec![1, 1, 0, 0, 1],
      },
      Atom {
        value: vec![0, 0, 0, 1, 0],
      },
      Atom {
        value: vec![0, 1, 0, 1, 0],
      },
    ];
    let expected_data = Cell {
      gamma: 22,
      epsilon: 9,
      gamma_str: String::from("10110"),
      epsilon_str: String::from("01001"),
    };
    assert_eq!(process_atoms(input_data), expected_data)
  }
}
