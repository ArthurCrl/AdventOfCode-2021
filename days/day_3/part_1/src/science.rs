#[derive(Debug, PartialEq)]
pub struct Atom {
  pub value: Vec<u32>,
}

#[derive(Debug, PartialEq)]
pub struct Molecule {
  pub number_of_one_bits: u64,
  pub number_of_zero_bits: u64
}

#[derive(Debug, PartialEq)]
pub struct Cell {
  pub gamma: u64,
  pub epsilon: u64,
  pub gamma_str: String,
  pub epsilon_str: String,
}
