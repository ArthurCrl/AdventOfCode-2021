#[derive(Debug, PartialEq, Clone)]
pub struct Atom {
  pub timer: usize,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Molecule {
  pub number_of_fish: usize,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Cell {}
