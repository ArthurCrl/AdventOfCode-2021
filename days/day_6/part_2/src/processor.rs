use crate::science::Atom;
use crate::science::Molecule;

pub fn process_atoms(atoms: Vec<Atom>, max_iter: usize) -> usize {
  let mut molecules = vec![
    Molecule { number_of_fish: 0 },
    Molecule { number_of_fish: 0 },
    Molecule { number_of_fish: 0 },
    Molecule { number_of_fish: 0 },
    Molecule { number_of_fish: 0 },
    Molecule { number_of_fish: 0 },
    Molecule { number_of_fish: 0 },
    Molecule { number_of_fish: 0 },
    Molecule { number_of_fish: 0 },
  ];
  for atom in atoms {
    molecules[atom.timer].number_of_fish = molecules[atom.timer].number_of_fish + 1;
  }
  for _i in 0..max_iter {
    let new_fish = molecules[0].number_of_fish;
    for time_left in 0..8 {
      molecules[time_left] = molecules[time_left + 1].clone();
    }
    molecules[8].number_of_fish = new_fish;
    molecules[6].number_of_fish += new_fish;
  }
  let mut total = 0;
  for molecule in molecules {
    total = total + molecule.number_of_fish;
  }
  return total;
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn validate_atom_processing() {
    let input_data = vec![
      Atom { timer: 3 },
      Atom { timer: 4 },
      Atom { timer: 3 },
      Atom { timer: 1 },
      Atom { timer: 2 },
    ];
    assert_eq!(process_atoms(input_data, 18), 26);
  }

  #[test]
  fn validate_atom_processing_high() {
    let input_data = vec![
      Atom { timer: 3 },
      Atom { timer: 4 },
      Atom { timer: 3 },
      Atom { timer: 1 },
      Atom { timer: 2 },
    ];
    assert_eq!(process_atoms(input_data, 80), 5934);
  }

  #[test]
  fn validate_atom_processing_very_high() {
    let input_data = vec![
      Atom { timer: 3 },
      Atom { timer: 4 },
      Atom { timer: 3 },
      Atom { timer: 1 },
      Atom { timer: 2 },
    ];
    assert_eq!(process_atoms(input_data, 256), 26984457539);
  }
}
