#[derive(Debug, PartialEq, Clone)]
pub struct Atom {
  pub timer: usize,
}

#[derive(Debug, PartialEq)]
pub struct Molecule {}

#[derive(Debug, PartialEq, Clone)]
pub struct Cell {}
