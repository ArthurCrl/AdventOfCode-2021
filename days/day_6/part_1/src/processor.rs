use crate::science::Atom;

pub fn process_atoms(atoms: Vec<Atom>, max_iter: usize) -> usize {
  let mut current_atoms = atoms;
  for _i in 0..max_iter {
    let mut next_atoms = Vec::new();
    for mut atom in current_atoms {
      if atom.timer == 0 {
        atom.timer = 6;
        next_atoms.push(atom.clone());
        next_atoms.push(Atom { timer: 8 });
      } else {
        atom.timer = atom.timer - 1;
        next_atoms.push(atom);
      }
    }
    current_atoms = next_atoms;
  }
  return current_atoms.len();
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn validate_atom_processing() {
    let input_data = vec![
      Atom { timer: 3 },
      Atom { timer: 4 },
      Atom { timer: 3 },
      Atom { timer: 1 },
      Atom { timer: 2 },
    ];
    assert_eq!(process_atoms(input_data, 18), 26);
  }

  #[test]
  fn validate_atom_processing_high() {
    let input_data = vec![
      Atom { timer: 3 },
      Atom { timer: 4 },
      Atom { timer: 3 },
      Atom { timer: 1 },
      Atom { timer: 2 },
    ];
    assert_eq!(process_atoms(input_data, 80), 5934);
  }
}
