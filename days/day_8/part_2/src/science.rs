#[derive(Debug, PartialEq, Clone, PartialOrd, Ord, Eq)]
pub struct Atom {
    pub signals: [Vec<Letter>; 10],
    pub digits: [Vec<Letter>; 4],
}

#[derive(Debug, PartialEq, Clone, PartialOrd, Ord, Eq, Hash)]

pub enum Letter {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
}


#[derive(Debug, PartialEq)]
pub struct Molecule {}

#[derive(Debug, PartialEq, Clone)]
pub struct Cell {}
