use crate::science::Atom;
use crate::science::Letter::A;
use crate::science::Letter::B;
use crate::science::Letter::C;
use crate::science::Letter::D;
use crate::science::Letter::E;
use crate::science::Letter::F;
use crate::science::Letter::G;
use regex::Regex;
use std::fs;
use std::io::Read;
use std::path::Path;

pub fn read_file_input(path: &str) -> String {
    let mut file =
        fs::File::open(Path::new(path)).expect("An error occurred while opening the file");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("An error occurred while reading the file");
    return contents;
}

pub fn parse_file_content(data: &String) -> Vec<Atom> {
    let mut parsed_data = Vec::new();
    for (_index, line) in data.lines().enumerate() {
        let parsed_atom = parse_line(line);
        parsed_data.push(parsed_atom)
    }
    return parsed_data;
}

fn parse_line(line: &str) -> Atom {
    let regex = Regex::new(r"([a-h]{1,7})").unwrap();
    let captures = regex.captures_iter(line);
    let mut signals = Vec::new();
    for c in captures {
        let chars = c.get(0).unwrap().as_str().chars();
        let mut signal = Vec::new();
        for car in chars {
            if car == 'a' {
                signal.push(A)
            }
            if car == 'b' {
                signal.push(B)
            }
            if car == 'c' {
                signal.push(C)
            }
            if car == 'd' {
                signal.push(D)
            }
            if car == 'e' {
                signal.push(E)
            }
            if car == 'f' {
                signal.push(F)
            }
            if car == 'g' {
                signal.push(G)
            }
        }
        signals.push(signal);
    }
    let atom = Atom {
        signals: 
[
            signals[0].clone(),
            signals[1].clone(),
            signals[2].clone(),
            signals[3].clone(),
            signals[4].clone(),
            signals[5].clone(),
            signals[6].clone(),
            signals[7].clone(),
            signals[8].clone(),
            signals[9].clone(),
        ],
        digits: 
[
            signals[10].clone(),
            signals[11].clone(),
            signals[12].clone(),
            signals[13].clone(),
        ],
    };
    return atom;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_parse_standard_line_correctly() {
        assert_eq!(
      parse_line("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"),
      Atom {
        signals: [
          vec![A, C, E, D, G, F, B], 
          vec![C, D, F, B, E], 
          vec![G, C, D, F, A], 
          vec![F, B, C, A, D], 
          vec![D, A, B], 
          vec![C, E, F, A, B, D], 
          vec![C, D, F, G, E, B], 
          vec![E, A, F, B], 
          vec![C, A, G, E, D, B], 
          vec![A, B]], 
        digits: [
          vec![C, D, F, E, B],
          vec![F, C, A, D, B], 
          vec![C, D, F, E, B],
          vec![C, D, B, A, F]
        ] ,
      });
    }

    #[test]
    fn should_parse_sample_file_correctly() {
        assert_eq!(
            parse_file_content(&read_file_input("./input/sample.txt")),
            vec![
              Atom { 
                signals: [
                  vec![B, E],
                  vec![C, F, B, E, G, A, D],
                  vec![C, B, D, G, E, F],
                  vec![F, G, A, E, C, D],
                  vec![C, G, E, B],
                  vec![F, D, C, G, E],
                  vec![A, G, E, B, F, D],
                  vec![F, E, C, D, B],
                  vec![F, A, B, C, D],
                  vec![E, D, B]
                ], 
                digits: [
                  vec![F, D, G, A, C, B, E],
                  vec![C, E, F, D, B],
                  vec![C, E, F, B, G, D],
                  vec![G, C, B, E]
                ]
              },
              Atom { 
                signals: [
                  vec![E, D, B, F, G, A], 
                  vec![B, E, G, C, D], 
                  vec![C, B, G],
                  vec![G, C],
                  vec![G, C, A, D, E, B, F],
                  vec![F, B, G, D, E],
                  vec![A, C, B, G, F, D],
                  vec![A, B, C, D, E],
                  vec![G, F, C, B, E, D],
                  vec![G, F, E, C]
                ],
                digits: [
                  vec![F, C, G, E, D, B],
                  vec![C, G, B],
                  vec![D, G, E, B, A, C, F],
                  vec![G, C]
                ]
              }, 
              Atom { 
                signals: [
                  vec![F, G, A, E, B, D], 
                  vec![C, G], 
                  vec![B, D, A, E, C], 
                  vec![G, D, A, F, B], 
                  vec![A, G, B, C, F, D], 
                  vec![G, D, C, B, E, F], 
                  vec![B, G, C, A, D], 
                  vec![G, F, A, C], 
                  vec![G, C, B], 
                  vec![C, D, G, A, B, E, F]], 
                digits: [
                  vec![C, G], 
                  vec![C, G], 
                  vec![F, D, C, A, G, B], 
                  vec![C, B, G]
                ]
              },
              Atom {
                signals: [
                  vec![F, B, E, G, C, D], 
                  vec![C, B, D], 
                  vec![A, D, C, E, F, B], 
                  vec![D, A, G, E, B], 
                  vec![A, F, C, B], 
                  vec![B, C], 
                  vec![A, E, F, D, C], 
                  vec![E, C, D, A, B], 
                  vec![F, G, D, E, C, A], 
                  vec![F, C, D, B, E, G, A]],
                digits: [
                  vec![E, F, A, B, C, D], 
                  vec![C, E, D, B, A], 
                  vec![G, A, D, F, E, C], 
                  vec![C, B]
                ]
              },
              Atom { 
                signals: [
                  vec![A, E, C, B, F, D, G], 
                  vec![F, B, G], 
                  vec![G, F], 
                  vec![B, A, F, E, G], 
                  vec![D, B, E, F, A], 
                  vec![F, C, G, E], 
                  vec![G, C, B, E, A], 
                  vec![F, C, A, E, G, B], 
                  vec![D, G, C, E, A, B], 
                  vec![F, C, B, D, G, A]],
                digits: [
                  vec![G, E, C, F], 
                  vec![E, G, D, C, A, B, F], 
                  vec![B, G, F], 
                  vec![B, F, G, E, A]] },
              Atom { 
                signals: [
                  vec![F, G, E, A, B], 
                  vec![C, A], 
                  vec![A, F, C, E, B, G], 
                  vec![B, D, A, C, F, E, G], 
                  vec![C, F, A, E, D, G], 
                  vec![G, C, F, D, B], 
                  vec![B, A, E, C], 
                  vec![B, F, A, D, E, G], 
                  vec![B, A, F, G, C], 
                  vec![A, C, F]
                ], 
                digits: [
                  vec![G, E, B, D, C, F, A], 
                  vec![E, C, B, A], 
                  vec![C, A], 
                  vec![F, A, D, E, G, C, B]
                ]
              },
              Atom { 
                signals: [
                  vec![D, B, C, F, G], 
                  vec![F, G, D], 
                  vec![B, D, E, G, C, A, F], 
                  vec![F, G, E, C], 
                  vec![A, E, G, B, D, F], 
                  vec![E, C, D, F, A, B], 
                  vec![F, B, E, D, C], 
                  vec![D, A, C, G, B], 
                  vec![G, D, C, E, B, F], 
                  vec![G, F]
                ],
                digits: [
                  vec![C, E, F, G], 
                  vec![D, C, B, E, F], 
                  vec![F, C, G, E], 
                  vec![G, B, C, A, D, F, E]
              ] 
              },
              Atom { 
                signals: [
                  vec![B, D, F, E, G, C], 
                  vec![C, B, E, G, A, F], 
                  vec![G, E, C, B, F], 
                  vec![D, F, C, A, G, E], 
                  vec![B, D, A, C, G], 
                  vec![E, D], 
                  vec![B, E, D, F], 
                  vec![C, E, D], 
                  vec![A, D, C, B, E, F, G], 
                  vec![G, E, B, C, D]], 
                digits: [
                  vec![E, D], 
                  vec![B, C, G, A, F, E], 
                  vec![C, D, G, B, A], 
                  vec![C, B, G, E, F]
                ]
              },
              Atom { 
                signals: [
                  vec![E, G, A, D, F, B], 
                  vec![C, D, B, F, E, G], 
                  vec![C, E, G, D], 
                  vec![F, E, C, A, B], 
                  vec![C, G, B], 
                  vec![G, B, D, E, F, C, A], 
                  vec![C, G], 
                  vec![F, G, C, D, A, B], 
                  vec![E, G, F, D, B], 
                  vec![B, F, C, E, G]],
                digits: [
                  vec![G, B, D, F, C, A, E], 
                  vec![B, G, C], 
                  vec![C, G], 
                  vec![C, G, B]
                ]
              },
              Atom { 
                signals: [
                  vec![G, C, A, F, B], 
                  vec![G, C, F], 
                  vec![D, C, A, E, B, F, G], 
                  vec![E, C, A, G, B], 
                  vec![G, F], 
                  vec![A, B, C, D, E, G], 
                  vec![G, A, E, F], 
                  vec![C, A, F, B, G, E], 
                  vec![F, D, B, A, C], 
                  vec![F, E, G, B, D, C]
                ],
                digits: [
                  vec![F, G, A, E], 
                  vec![C, F, G, A, B], 
                  vec![F, G], 
                  vec![B, A, G, C, E]
                ]
              }]
            );
        }
    }
