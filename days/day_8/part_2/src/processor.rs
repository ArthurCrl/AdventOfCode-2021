use crate::science::Atom;
use crate::science::Letter;
use std::collections::HashSet;

pub fn process_atoms(atoms: Vec<Atom>) -> usize {
    let mut cmpt = 0;
    for atom in atoms {
        let numbers = find_numbers(atom.clone());
        let code = find_code(atom, numbers);
        cmpt = cmpt + code;
    }
    return cmpt;
}

fn find_code(atom: Atom, numbers: [Vec<Letter>; 10]) -> usize {
    let digits = atom.digits;
    let mut code = 0;
    let mut power = 4;
    for mut digit in digits {
        power = power - 1;
        for number in 0..numbers.len() {
            let mut signal = (*numbers.get(number).unwrap()).clone();
            signal.sort();
            digit.sort();
            if signal == digit {
                code = code + 10_usize.pow(power)*number;
            }
        }
    }
    return code;
}

fn find_numbers(atom: Atom) -> [Vec<Letter>; 10] {
    let mut found_numbers = Vec::new();
    let mut signals: [Vec<Letter>; 10] = atom.signals.clone();
    signals.sort_by(|a, b| a.len().cmp(&b.len()));
    let mut length_five = Vec::new();
    let mut length_six = Vec::new();
    for signal in signals {
        if signal.len() == 2 {
            // 1
            found_numbers.push(signal.clone());
        }
        if signal.len() == 3 {
            // 7
            found_numbers.push(signal.clone());
        }
        if signal.len() == 4 {
            // 4
            found_numbers.push(signal.clone());
        }
        if signal.len() == 7 {
            // 8
            found_numbers.push(signal.clone());
        }
        if signal.len() == 5 {
            // 2,3,5
            length_five.push(signal.clone());
        }
        if signal.len() == 6 {
            // 0,6,9
            length_six.push(signal.clone());
        }
    }
    let mut one_signal = found_numbers[0].clone();
    let one_hash: HashSet<&Letter> = one_signal.iter().collect();
    let mut seven_signal = found_numbers[1].clone();
    let mut four_signal = found_numbers[2].clone();
    let four_hash: HashSet<&Letter> = four_signal.iter().collect();
    let mut height_signal = found_numbers[3].clone();
    let mut remaining_length_five = Vec::new();
    for signal in length_five {
        let current_hashset: HashSet<&Letter> = signal.iter().collect();
        let minus_one: Vec<&&Letter> = current_hashset
            .symmetric_difference(&one_hash)
            .into_iter()
            .collect();
        if minus_one.len() == 3 {
            found_numbers.push(signal);
        } else {
            remaining_length_five.push(signal);
        }
    }
    let mut three_signal = found_numbers[4].clone();
    let current_hashset: HashSet<&Letter> = remaining_length_five[0].iter().collect(); // 2,5
    let minus_four: Vec<&&Letter> = current_hashset.difference(&four_hash).into_iter().collect();
    if minus_four.len() == 2 {
        found_numbers.push(remaining_length_five[0].clone()); //5
        found_numbers.push(remaining_length_five[1].clone()); //2
    } else {
        found_numbers.push(remaining_length_five[1].clone()); //5
        found_numbers.push(remaining_length_five[0].clone()); //2
    }
    let mut five_signal = found_numbers[5].clone();
    let two_signal = found_numbers[6].clone();
    let two_hash: HashSet<&Letter> = two_signal.iter().collect();

    let mut remaining_length_six = Vec::new();
    for signal in length_six {
        let current_hashset: HashSet<&Letter> = signal.iter().collect();
        let minus_one: Vec<&&Letter> = current_hashset.difference(&one_hash).into_iter().collect();
        if minus_one.len() == 5 {
            found_numbers.push(signal);
        } else {
            remaining_length_six.push(signal);
        }
    }
    let mut six_signal = found_numbers[7].clone();
    let current_hashset: HashSet<&Letter> = remaining_length_six[0].iter().collect(); // 9,0
    let minus_four: Vec<&&Letter> = current_hashset
        .symmetric_difference(&four_hash)
        .into_iter()
        .collect();
    if minus_four.len() == 2 {
        found_numbers.push(remaining_length_six[0].clone()); //9
        found_numbers.push(remaining_length_six[1].clone()); //0
    } else {
        found_numbers.push(remaining_length_six[1].clone()); //9
        found_numbers.push(remaining_length_six[0].clone()); //0
    }
    let mut nine_signal = found_numbers[8].clone();

    let mut zero_signal = found_numbers[9].clone();
    let mut two_signal: Vec<Letter> = two_hash.into_iter().map(|a| (*a).clone()).collect();

    zero_signal.sort();
    one_signal.sort();
    two_signal.sort();
    three_signal.sort();
    four_signal.sort();
    five_signal.sort();
    six_signal.sort();
    seven_signal.sort();
    height_signal.sort();
    nine_signal.sort();
    return [
        zero_signal,
        one_signal,
        two_signal,
        three_signal,
        four_signal,
        five_signal,
        six_signal,
        seven_signal,
        height_signal,
        nine_signal,
    ];
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::science::Letter::A;
    use crate::science::Letter::B;
    use crate::science::Letter::C;
    use crate::science::Letter::D;
    use crate::science::Letter::E;
    use crate::science::Letter::F;
    use crate::science::Letter::G;

    #[test]
    fn validate_numbers_processing() {
        let input_data = Atom {
            signals: [
                vec![A, C, E, D, G, F, B],
                vec![C, D, F, B, E],
                vec![G, C, D, F, A],
                vec![F, B, C, A, D],
                vec![D, A, B],
                vec![C, E, F, A, B, D],
                vec![C, D, F, G, E, B],
                vec![E, A, F, B],
                vec![C, A, G, E, D, B],
                vec![A, B],
            ],
            digits: [
                vec![C, D, F, E, B],
                vec![F, C, A, D, B],
                vec![C, D, F, E, B],
                vec![C, D, B, A, F],
            ],
        };
        assert_eq!(
            find_numbers(input_data),
            [
                vec![A, B, C, D, E, G],
                vec![A, B],
                vec![A, C, D, F, G],
                vec![A, B, C, D, F],
                vec![A, B, E, F],
                vec![B, C, D, E, F],
                vec![B, C, D, E, F, G],
                vec![A, B, D],
                vec![A, B, C, D, E, F, G],
                vec![A, B, C, D, E, F]
            ]
        );
    }

    #[test]
    fn validate_code_processing() {
        let input_data_1 = Atom {
            signals: [
                vec![A, C, E, D, G, F, B],
                vec![C, D, F, B, E],
                vec![G, C, D, F, A],
                vec![F, B, C, A, D],
                vec![D, A, B],
                vec![C, E, F, A, B, D],
                vec![C, D, F, G, E, B],
                vec![E, A, F, B],
                vec![C, A, G, E, D, B],
                vec![A, B],
            ],
            digits: [
                vec![C, D, F, E, B],
                vec![F, C, A, D, B],
                vec![C, D, F, E, B],
                vec![C, D, B, A, F],
            ],
        };
        let input_data_2 = [
            vec![A, B, C, D, E, G],
            vec![A, B],
            vec![A, C, D, F, G],
            vec![A, B, C, D, F],
            vec![A, B, E, F],
            vec![B, C, D, E, F],
            vec![B, C, D, E, F, G],
            vec![A, B, D],
            vec![A, B, C, D, E, F, G],
            vec![A, B, C, D, E, F],
        ];
        assert_eq!(find_code(input_data_1,input_data_2), 5353);
    }
    #[test]
    fn validate_atoms_processing() {
        let input_data = vec![
            Atom {
                signals: [
                    vec![B, E],
                    vec![C, F, B, E, G, A, D],
                    vec![C, B, D, G, E, F],
                    vec![F, G, A, E, C, D],
                    vec![C, G, E, B],
                    vec![F, D, C, G, E],
                    vec![A, G, E, B, F, D],
                    vec![F, E, C, D, B],
                    vec![F, A, B, C, D],
                    vec![E, D, B],
                ],
                digits: [
                    vec![F, D, G, A, C, B, E],
                    vec![C, E, F, D, B],
                    vec![C, E, F, B, G, D],
                    vec![G, C, B, E],
                ],
            },
            Atom {
                signals: [
                    vec![E, D, B, F, G, A],
                    vec![B, E, G, C, D],
                    vec![C, B, G],
                    vec![G, C],
                    vec![G, C, A, D, E, B, F],
                    vec![F, B, G, D, E],
                    vec![A, C, B, G, F, D],
                    vec![A, B, C, D, E],
                    vec![G, F, C, B, E, D],
                    vec![G, F, E, C],
                ],
                digits: [
                    vec![F, C, G, E, D, B],
                    vec![C, G, B],
                    vec![D, G, E, B, A, C, F],
                    vec![G, C],
                ],
            },
            Atom {
                signals: [
                    vec![F, G, A, E, B, D],
                    vec![C, G],
                    vec![B, D, A, E, C],
                    vec![G, D, A, F, B],
                    vec![A, G, B, C, F, D],
                    vec![G, D, C, B, E, F],
                    vec![B, G, C, A, D],
                    vec![G, F, A, C],
                    vec![G, C, B],
                    vec![C, D, G, A, B, E, F],
                ],
                digits: [
                    vec![C, G],
                    vec![C, G],
                    vec![F, D, C, A, G, B],
                    vec![C, B, G],
                ],
            },
            Atom {
                signals: [
                    vec![F, B, E, G, C, D],
                    vec![C, B, D],
                    vec![A, D, C, E, F, B],
                    vec![D, A, G, E, B],
                    vec![A, F, C, B],
                    vec![B, C],
                    vec![A, E, F, D, C],
                    vec![E, C, D, A, B],
                    vec![F, G, D, E, C, A],
                    vec![F, C, D, B, E, G, A],
                ],
                digits: [
                    vec![E, F, A, B, C, D],
                    vec![C, E, D, B, A],
                    vec![G, A, D, F, E, C],
                    vec![C, B],
                ],
            },
            Atom {
                signals: [
                    vec![A, E, C, B, F, D, G],
                    vec![F, B, G],
                    vec![G, F],
                    vec![B, A, F, E, G],
                    vec![D, B, E, F, A],
                    vec![F, C, G, E],
                    vec![G, C, B, E, A],
                    vec![F, C, A, E, G, B],
                    vec![D, G, C, E, A, B],
                    vec![F, C, B, D, G, A],
                ],
                digits: [
                    vec![G, E, C, F],
                    vec![E, G, D, C, A, B, F],
                    vec![B, G, F],
                    vec![B, F, G, E, A],
                ],
            },
            Atom {
                signals: [
                    vec![F, G, E, A, B],
                    vec![C, A],
                    vec![A, F, C, E, B, G],
                    vec![B, D, A, C, F, E, G],
                    vec![C, F, A, E, D, G],
                    vec![G, C, F, D, B],
                    vec![B, A, E, C],
                    vec![B, F, A, D, E, G],
                    vec![B, A, F, G, C],
                    vec![A, C, F],
                ],
                digits: [
                    vec![G, E, B, D, C, F, A],
                    vec![E, C, B, A],
                    vec![C, A],
                    vec![F, A, D, E, G, C, B],
                ],
            },
            Atom {
                signals: [
                    vec![D, B, C, F, G],
                    vec![F, G, D],
                    vec![B, D, E, G, C, A, F],
                    vec![F, G, E, C],
                    vec![A, E, G, B, D, F],
                    vec![E, C, D, F, A, B],
                    vec![F, B, E, D, C],
                    vec![D, A, C, G, B],
                    vec![G, D, C, E, B, F],
                    vec![G, F],
                ],
                digits: [
                    vec![C, E, F, G],
                    vec![D, C, B, E, F],
                    vec![F, C, G, E],
                    vec![G, B, C, A, D, F, E],
                ],
            },
            Atom {
                signals: [
                    vec![B, D, F, E, G, C],
                    vec![C, B, E, G, A, F],
                    vec![G, E, C, B, F],
                    vec![D, F, C, A, G, E],
                    vec![B, D, A, C, G],
                    vec![E, D],
                    vec![B, E, D, F],
                    vec![C, E, D],
                    vec![A, D, C, B, E, F, G],
                    vec![G, E, B, C, D],
                ],
                digits: [
                    vec![E, D],
                    vec![B, C, G, A, F, E],
                    vec![C, D, G, B, A],
                    vec![C, B, G, E, F],
                ],
            },
            Atom {
                signals: [
                    vec![E, G, A, D, F, B],
                    vec![C, D, B, F, E, G],
                    vec![C, E, G, D],
                    vec![F, E, C, A, B],
                    vec![C, G, B],
                    vec![G, B, D, E, F, C, A],
                    vec![C, G],
                    vec![F, G, C, D, A, B],
                    vec![E, G, F, D, B],
                    vec![B, F, C, E, G],
                ],
                digits: [
                    vec![G, B, D, F, C, A, E],
                    vec![B, G, C],
                    vec![C, G],
                    vec![C, G, B],
                ],
            },
            Atom {
                signals: [
                    vec![G, C, A, F, B],
                    vec![G, C, F],
                    vec![D, C, A, E, B, F, G],
                    vec![E, C, A, G, B],
                    vec![G, F],
                    vec![A, B, C, D, E, G],
                    vec![G, A, E, F],
                    vec![C, A, F, B, G, E],
                    vec![F, D, B, A, C],
                    vec![F, E, G, B, D, C],
                ],
                digits: [
                    vec![F, G, A, E],
                    vec![C, F, G, A, B],
                    vec![F, G],
                    vec![B, A, G, C, E],
                ],
            },
        ];
        assert_eq!(process_atoms(input_data), 61229);
    }
}
