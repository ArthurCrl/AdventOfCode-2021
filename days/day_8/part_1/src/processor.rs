use crate::science::Atom;
pub fn process_atoms(atoms: Vec<Atom>) -> isize {
    let mut cmpt = 0;
    for atom in atoms {
        for digit in atom.digits {
            let dgt_len = digit.len();
            if dgt_len == 2 || dgt_len == 3 || dgt_len == 4 || dgt_len == 7 {
                cmpt = cmpt + 1;
            }
        }
    }
    return cmpt;
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::science::Letter::A;
    use crate::science::Letter::B;
    use crate::science::Letter::C;
    use crate::science::Letter::D;
    use crate::science::Letter::E;
    use crate::science::Letter::F;
    use crate::science::Letter::G;

    #[test]
    fn validate_atom_processing() {
        let input_data = vec![
            Atom {
                signals: [
                    vec![B, E],
                    vec![C, F, B, E, G, A, D],
                    vec![C, B, D, G, E, F],
                    vec![F, G, A, E, C, D],
                    vec![C, G, E, B],
                    vec![F, D, C, G, E],
                    vec![A, G, E, B, F, D],
                    vec![F, E, C, D, B],
                    vec![F, A, B, C, D],
                    vec![E, D, B],
                ],
                digits: [
                    vec![F, D, G, A, C, B, E],
                    vec![C, E, F, D, B],
                    vec![C, E, F, B, G, D],
                    vec![G, C, B, E],
                ],
            },
            Atom {
                signals: [
                    vec![E, D, B, F, G, A],
                    vec![B, E, G, C, D],
                    vec![C, B, G],
                    vec![G, C],
                    vec![G, C, A, D, E, B, F],
                    vec![F, B, G, D, E],
                    vec![A, C, B, G, F, D],
                    vec![A, B, C, D, E],
                    vec![G, F, C, B, E, D],
                    vec![G, F, E, C],
                ],
                digits: [
                    vec![F, C, G, E, D, B],
                    vec![C, G, B],
                    vec![D, G, E, B, A, C, F],
                    vec![G, C],
                ],
            },
            Atom {
                signals: [
                    vec![F, G, A, E, B, D],
                    vec![C, G],
                    vec![B, D, A, E, C],
                    vec![G, D, A, F, B],
                    vec![A, G, B, C, F, D],
                    vec![G, D, C, B, E, F],
                    vec![B, G, C, A, D],
                    vec![G, F, A, C],
                    vec![G, C, B],
                    vec![C, D, G, A, B, E, F],
                ],
                digits: [
                    vec![C, G],
                    vec![C, G],
                    vec![F, D, C, A, G, B],
                    vec![C, B, G],
                ],
            },
            Atom {
                signals: [
                    vec![F, B, E, G, C, D],
                    vec![C, B, D],
                    vec![A, D, C, E, F, B],
                    vec![D, A, G, E, B],
                    vec![A, F, C, B],
                    vec![B, C],
                    vec![A, E, F, D, C],
                    vec![E, C, D, A, B],
                    vec![F, G, D, E, C, A],
                    vec![F, C, D, B, E, G, A],
                ],
                digits: [
                    vec![E, F, A, B, C, D],
                    vec![C, E, D, B, A],
                    vec![G, A, D, F, E, C],
                    vec![C, B],
                ],
            },
            Atom {
                signals: [
                    vec![A, E, C, B, F, D, G],
                    vec![F, B, G],
                    vec![G, F],
                    vec![B, A, F, E, G],
                    vec![D, B, E, F, A],
                    vec![F, C, G, E],
                    vec![G, C, B, E, A],
                    vec![F, C, A, E, G, B],
                    vec![D, G, C, E, A, B],
                    vec![F, C, B, D, G, A],
                ],
                digits: [
                    vec![G, E, C, F],
                    vec![E, G, D, C, A, B, F],
                    vec![B, G, F],
                    vec![B, F, G, E, A],
                ],
            },
            Atom {
                signals: [
                    vec![F, G, E, A, B],
                    vec![C, A],
                    vec![A, F, C, E, B, G],
                    vec![B, D, A, C, F, E, G],
                    vec![C, F, A, E, D, G],
                    vec![G, C, F, D, B],
                    vec![B, A, E, C],
                    vec![B, F, A, D, E, G],
                    vec![B, A, F, G, C],
                    vec![A, C, F],
                ],
                digits: [
                    vec![G, E, B, D, C, F, A],
                    vec![E, C, B, A],
                    vec![C, A],
                    vec![F, A, D, E, G, C, B],
                ],
            },
            Atom {
                signals: [
                    vec![D, B, C, F, G],
                    vec![F, G, D],
                    vec![B, D, E, G, C, A, F],
                    vec![F, G, E, C],
                    vec![A, E, G, B, D, F],
                    vec![E, C, D, F, A, B],
                    vec![F, B, E, D, C],
                    vec![D, A, C, G, B],
                    vec![G, D, C, E, B, F],
                    vec![G, F],
                ],
                digits: [
                    vec![C, E, F, G],
                    vec![D, C, B, E, F],
                    vec![F, C, G, E],
                    vec![G, B, C, A, D, F, E],
                ],
            },
            Atom {
                signals: [
                    vec![B, D, F, E, G, C],
                    vec![C, B, E, G, A, F],
                    vec![G, E, C, B, F],
                    vec![D, F, C, A, G, E],
                    vec![B, D, A, C, G],
                    vec![E, D],
                    vec![B, E, D, F],
                    vec![C, E, D],
                    vec![A, D, C, B, E, F, G],
                    vec![G, E, B, C, D],
                ],
                digits: [
                    vec![E, D],
                    vec![B, C, G, A, F, E],
                    vec![C, D, G, B, A],
                    vec![C, B, G, E, F],
                ],
            },
            Atom {
                signals: [
                    vec![E, G, A, D, F, B],
                    vec![C, D, B, F, E, G],
                    vec![C, E, G, D],
                    vec![F, E, C, A, B],
                    vec![C, G, B],
                    vec![G, B, D, E, F, C, A],
                    vec![C, G],
                    vec![F, G, C, D, A, B],
                    vec![E, G, F, D, B],
                    vec![B, F, C, E, G],
                ],
                digits: [
                    vec![G, B, D, F, C, A, E],
                    vec![B, G, C],
                    vec![C, G],
                    vec![C, G, B],
                ],
            },
            Atom {
                signals: [
                    vec![G, C, A, F, B],
                    vec![G, C, F],
                    vec![D, C, A, E, B, F, G],
                    vec![E, C, A, G, B],
                    vec![G, F],
                    vec![A, B, C, D, E, G],
                    vec![G, A, E, F],
                    vec![C, A, F, B, G, E],
                    vec![F, D, B, A, C],
                    vec![F, E, G, B, D, C],
                ],
                digits: [
                    vec![F, G, A, E],
                    vec![C, F, G, A, B],
                    vec![F, G],
                    vec![B, A, G, C, E],
                ],
            },
        ];
        assert_eq!(process_atoms(input_data), 26);
    }
}
