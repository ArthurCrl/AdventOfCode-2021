#[derive(Debug, PartialEq, Clone, PartialOrd, Ord, Eq)]
pub struct Atom {
    pub signals: [Vec<Letter>; 10],
    pub digits: [Vec<Letter>; 4],
}

#[derive(Debug, PartialEq, Clone, PartialOrd, Ord, Eq)]

pub enum Letter {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
}

#[repr(u32)]
pub enum Number {
    One(u32) = 1,
    Two(u32) = 2,
    Three(u32) = 3,
    Four(u32) = 4,
    Five(u32) = 5,
    Six(u32) = 6,
    Seven(u32) = 7,
    Height(u32) = 8,
}

#[derive(Debug, PartialEq)]
pub struct Molecule {}

#[derive(Debug, PartialEq, Clone)]
pub struct Cell {}
