mod parser;
mod processor;
mod science;

fn main() {
    let file_content: String = parser::read_file_input("./input/input.txt");
    let molecules: science::Molecule = parser::parse_file_content(&file_content);
    let result: Vec<science::Cell> = processor::process_molecules(molecules);
    println!(
        "Results: {:?}",
        result
    )
}
