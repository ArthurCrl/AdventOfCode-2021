use crate::science::Cell;
use crate::science::Molecule;
use crate::science::Score;

pub fn process_molecules(mut molecule: Molecule) -> Vec<Cell> {
  let mut cells = vec![];
  for draw in molecule.draws {
    let boards = &mut molecule.boards;
    for board_index in 0..boards.len() {
      let board = &mut boards[board_index];
      if !board.has_won {
        let lines = &mut board.lines;
        for line_index in 0..lines.len() {
          let line = &mut lines[line_index];
          let slots = &mut line.slots;
          for slot_index in 0..slots.len() {
            let mut slot = &mut slots[slot_index];
            if slot.number == draw {
              println!(
                "Slot match {:?} on line {:?} for board number {:?} ",
                slot.number, line_index, board_index
              );
              slot.is_drawn = true;
              board.drawn_status.insert(slot.number, true);
            }
          }
        }
        let board = &mut boards[board_index];
        if board.is_board_winning() {
          let mut winning_board = Cell {
            score: 0,
            winning_draw: 0,
          };
          winning_board.winning_draw = draw;
          winning_board.score = board.get_score();
          cells.push(winning_board);
          board.has_won = true;
        }
      }
    }
  }
  return cells;
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::science::Line;
  use crate::science::Slot;
  use std::collections::HashMap;
  use crate::science::Atom;
  #[test]
  fn validate_atom_processing() {
    let input_data = Molecule {
      draws: vec![
        7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16, 13, 6, 15, 25, 12, 22, 18, 20, 8, 19, 3,
        26, 1,
      ],
      boards: vec![
        Atom {
          has_won: false,
          lines: vec![
            Line {
              slots: vec![
                Slot {
                  number: 22,
                  is_drawn: false,
                },
                Slot {
                  number: 13,
                  is_drawn: false,
                },
                Slot {
                  number: 17,
                  is_drawn: false,
                },
                Slot {
                  number: 11,
                  is_drawn: false,
                },
                Slot {
                  number: 0,
                  is_drawn: false,
                },
              ],
            },
            Line {
              slots: vec![
                Slot {
                  number: 8,
                  is_drawn: false,
                },
                Slot {
                  number: 2,
                  is_drawn: false,
                },
                Slot {
                  number: 23,
                  is_drawn: false,
                },
                Slot {
                  number: 4,
                  is_drawn: false,
                },
                Slot {
                  number: 24,
                  is_drawn: false,
                },
              ],
            },
            Line {
              slots: vec![
                Slot {
                  number: 21,
                  is_drawn: false,
                },
                Slot {
                  number: 9,
                  is_drawn: false,
                },
                Slot {
                  number: 14,
                  is_drawn: false,
                },
                Slot {
                  number: 16,
                  is_drawn: false,
                },
                Slot {
                  number: 7,
                  is_drawn: false,
                },
              ],
            },
            Line {
              slots: vec![
                Slot {
                  number: 6,
                  is_drawn: false,
                },
                Slot {
                  number: 10,
                  is_drawn: false,
                },
                Slot {
                  number: 3,
                  is_drawn: false,
                },
                Slot {
                  number: 18,
                  is_drawn: false,
                },
                Slot {
                  number: 5,
                  is_drawn: false,
                },
              ],
            },
            Line {
              slots: vec![
                Slot {
                  number: 1,
                  is_drawn: false,
                },
                Slot {
                  number: 12,
                  is_drawn: false,
                },
                Slot {
                  number: 20,
                  is_drawn: false,
                },
                Slot {
                  number: 15,
                  is_drawn: false,
                },
                Slot {
                  number: 19,
                  is_drawn: false,
                },
              ],
            },
          ],
          drawn_status: HashMap::from([
            (11, false),
            (20, false),
            (22, false),
            (21, false),
            (9, false),
            (6, false),
            (8, false),
            (23, false),
            (2, false),
            (10, false),
            (13, false),
            (16, false),
            (3, false),
            (5, false),
            (15, false),
            (7, false),
            (17, false),
            (1, false),
            (4, false),
            (24, false),
            (14, false),
            (18, false),
            (0, false),
            (12, false),
            (19, false),
          ]),
        },
        Atom {
          has_won: false,
          lines: vec![
            Line {
              slots: vec![
                Slot {
                  number: 3,
                  is_drawn: false,
                },
                Slot {
                  number: 15,
                  is_drawn: false,
                },
                Slot {
                  number: 0,
                  is_drawn: false,
                },
                Slot {
                  number: 2,
                  is_drawn: false,
                },
                Slot {
                  number: 22,
                  is_drawn: false,
                },
              ],
            },
            Line {
              slots: vec![
                Slot {
                  number: 9,
                  is_drawn: false,
                },
                Slot {
                  number: 18,
                  is_drawn: false,
                },
                Slot {
                  number: 13,
                  is_drawn: false,
                },
                Slot {
                  number: 17,
                  is_drawn: false,
                },
                Slot {
                  number: 5,
                  is_drawn: false,
                },
              ],
            },
            Line {
              slots: vec![
                Slot {
                  number: 19,
                  is_drawn: false,
                },
                Slot {
                  number: 8,
                  is_drawn: false,
                },
                Slot {
                  number: 7,
                  is_drawn: false,
                },
                Slot {
                  number: 25,
                  is_drawn: false,
                },
                Slot {
                  number: 23,
                  is_drawn: false,
                },
              ],
            },
            Line {
              slots: vec![
                Slot {
                  number: 20,
                  is_drawn: false,
                },
                Slot {
                  number: 11,
                  is_drawn: false,
                },
                Slot {
                  number: 10,
                  is_drawn: false,
                },
                Slot {
                  number: 24,
                  is_drawn: false,
                },
                Slot {
                  number: 4,
                  is_drawn: false,
                },
              ],
            },
            Line {
              slots: vec![
                Slot {
                  number: 14,
                  is_drawn: false,
                },
                Slot {
                  number: 21,
                  is_drawn: false,
                },
                Slot {
                  number: 16,
                  is_drawn: false,
                },
                Slot {
                  number: 12,
                  is_drawn: false,
                },
                Slot {
                  number: 6,
                  is_drawn: false,
                },
              ],
            },
          ],
          drawn_status: HashMap::from([
            (23, false),
            (12, false),
            (8, false),
            (7, false),
            (20, false),
            (11, false),
            (4, false),
            (14, false),
            (19, false),
            (18, false),
            (22, false),
            (25, false),
            (9, false),
            (6, false),
            (3, false),
            (21, false),
            (16, false),
            (24, false),
            (10, false),
            (5, false),
            (0, false),
            (17, false),
            (13, false),
            (2, false),
            (15, false),
          ]),
        },
        Atom {
          has_won: false,
          lines: vec![
            Line {
              slots: vec![
                Slot {
                  number: 14,
                  is_drawn: false,
                },
                Slot {
                  number: 21,
                  is_drawn: false,
                },
                Slot {
                  number: 17,
                  is_drawn: false,
                },
                Slot {
                  number: 24,
                  is_drawn: false,
                },
                Slot {
                  number: 4,
                  is_drawn: false,
                },
              ],
            },
            Line {
              slots: vec![
                Slot {
                  number: 10,
                  is_drawn: false,
                },
                Slot {
                  number: 16,
                  is_drawn: false,
                },
                Slot {
                  number: 15,
                  is_drawn: false,
                },
                Slot {
                  number: 9,
                  is_drawn: false,
                },
                Slot {
                  number: 19,
                  is_drawn: false,
                },
              ],
            },
            Line {
              slots: vec![
                Slot {
                  number: 18,
                  is_drawn: false,
                },
                Slot {
                  number: 8,
                  is_drawn: false,
                },
                Slot {
                  number: 23,
                  is_drawn: false,
                },
                Slot {
                  number: 26,
                  is_drawn: false,
                },
                Slot {
                  number: 20,
                  is_drawn: false,
                },
              ],
            },
            Line {
              slots: vec![
                Slot {
                  number: 22,
                  is_drawn: false,
                },
                Slot {
                  number: 11,
                  is_drawn: false,
                },
                Slot {
                  number: 13,
                  is_drawn: false,
                },
                Slot {
                  number: 6,
                  is_drawn: false,
                },
                Slot {
                  number: 5,
                  is_drawn: false,
                },
              ],
            },
            Line {
              slots: vec![
                Slot {
                  number: 2,
                  is_drawn: false,
                },
                Slot {
                  number: 0,
                  is_drawn: false,
                },
                Slot {
                  number: 12,
                  is_drawn: false,
                },
                Slot {
                  number: 3,
                  is_drawn: false,
                },
                Slot {
                  number: 7,
                  is_drawn: false,
                },
              ],
            },
          ],
          drawn_status: HashMap::from([
            (20, false),
            (21, false),
            (2, false),
            (15, false),
            (14, false),
            (16, false),
            (13, false),
            (17, false),
            (18, false),
            (6, false),
            (5, false),
            (24, false),
            (9, false),
            (22, false),
            (11, false),
            (3, false),
            (0, false),
            (23, false),
            (26, false),
            (10, false),
            (4, false),
            (19, false),
            (8, false),
            (12, false),
            (7, false),
          ]),
        },
      ],
    };
    let expected_data = vec![
      Cell {
        winning_draw: 24,
        score: 188,
      },
      Cell {
        winning_draw: 16,
        score: 137,
      },
      Cell {
        winning_draw: 13,
        score: 148,
      },
    ];
    assert_eq!(process_molecules(input_data), expected_data)
  }
}
