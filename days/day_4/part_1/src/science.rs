use std::collections::HashMap;

#[derive(Debug, PartialEq, Clone)]
pub struct Atom {
  pub lines: Vec<Line>,
  pub drawn_status: HashMap<u32, bool>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Line {
  pub slots: Vec<Slot>,
}

pub trait Score {
  fn get_score(&self) -> u32;
  fn is_board_winning(&self) -> bool;
}

impl Score for Atom {
  fn get_score(&self) -> u32 {
    let mut score = 0;
    for (slot, status) in self.drawn_status.clone() {
      if !status {
        score = score + slot;
      }
    }
    return score;
  }

  fn is_board_winning(&self) -> bool {
    let mut is_column_full = true;
    let mut is_line_full = false;
    let first_line = &self.lines[0];
    for index in 0..first_line.slots.len() {
      for line in self.lines.clone() {
        let slot = &line.slots[index];
        if !slot.is_drawn {
          is_column_full = false;
          break;
        }
      }
      if is_column_full {
        break;
      }
    }
    for line in &self.lines {
      if line.is_full() {
        is_line_full = true;
        break;
      }
    }
    return is_column_full || is_line_full;
  }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Slot {
  pub number: u32,
  pub is_drawn: bool,
}

pub trait IsFull {
  fn is_full(&self) -> bool;
}

impl IsFull for Line {
  fn is_full(&self) -> bool {
    let mut is_full = true;
    for slot in self.slots.clone() {
      if !slot.is_drawn {
        is_full = false;
        break;
      }
    }
    return is_full;
  }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Index {
  pub line: u32,
  pub column: u32,
}

#[derive(Debug, PartialEq)]
pub struct Molecule {
  pub draws: Vec<u32>,
  pub boards: Vec<Atom>,
}

#[derive(Debug, PartialEq)]
pub struct Cell {
  pub winning_draw: u32,
  pub score: u32,
}
