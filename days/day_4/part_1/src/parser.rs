use crate::science::Atom;
use crate::science::Line;
use crate::science::Molecule;
use crate::science::Slot;
use regex::Regex;
use std::collections::HashMap;
use std::fs;
use std::io::Read;
use std::path::Path;

pub fn read_file_input(path: &str) -> String {
  let mut file = fs::File::open(Path::new(path)).expect("An error occurred while opening the file");
  let mut contents = String::new();
  file
    .read_to_string(&mut contents)
    .expect("An error occurred while reading the file");
  return contents;
}

pub fn parse_file_content(data: &String) -> Molecule {
  let mut parsed_data = Molecule {
    draws: vec![],
    boards: vec![],
  };
  let mut draws = Vec::new();
  let mut boards = Vec::new();

  let mut current_board = Atom {
    lines: vec![],
    drawn_status: HashMap::new(),
  };
  for (index, line) in data.lines().enumerate() {
    if index == 0 {
      parse_first_line(line, &mut draws)
    } else if index != 1 {
      let (parsed_line, should_create_new_board) = parse_line(line);
      if should_create_new_board {
        boards.push(current_board.clone());
        current_board = Atom {
          lines: vec![],
          drawn_status: HashMap::new(),
        };
      } else {
        for slot in parsed_line.slots.clone() {
          let _old_val = current_board.drawn_status.insert(slot.number, false);
        }
        current_board.lines.push(parsed_line)
      }
    }
  }
  boards.push(current_board.clone());
  parsed_data.draws = draws;
  parsed_data.boards = boards;
  return parsed_data;
}

fn parse_first_line(line: &str, draws: &mut Vec<u32>) {
  let regex = Regex::new(r"(?:(\d+),?)").unwrap();
  let captures = regex.captures_iter(line);
  for c in captures {
    draws.push(c[1].parse().unwrap())
  }
}

fn parse_line(line: &str) -> (Line, bool) {
  if line == "" {
    return (Line { slots: vec![] }, true);
  } else {
    let regex = Regex::new(r"(?:(\d+) ?)").unwrap();
    let captures = regex.captures_iter(line);
    let mut new_line = Line { slots: vec![] };
    for c in captures {
      new_line.slots.push(Slot {
        number: c[1].parse().unwrap(),
        is_drawn: false,
      })
    }
    return (new_line, false);
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn should_parse_first_line_correctly() {
    let mut draws = Vec::new();
    parse_first_line("4,5,27,3", &mut draws);
    assert_eq!(draws, [4, 5, 27, 3]);
  }

  #[test]
  fn should_parse_standard_line_correctly() {
    assert_eq!(parse_line("",), (Line { slots: vec![] }, true));
    assert_eq!(
      parse_line("4 5 27 3"),
      (
        Line {
          slots: vec![
            Slot {
              number: 4,
              is_drawn: false,
            },
            Slot {
              number: 5,
              is_drawn: false,
            },
            Slot {
              number: 27,
              is_drawn: false,
            },
            Slot {
              number: 3,
              is_drawn: false,
            }
          ]
        },
        false
      )
    );
  }

  #[test]
  fn should_parse_sample_file_correctly() {
    assert_eq!(
      parse_file_content(&read_file_input("./input/sample.txt")),
      Molecule {
        draws: vec![
          7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16, 13, 6, 15, 25, 12, 22, 18, 20, 8, 19,
          3, 26, 1
        ],
        boards: vec![
          Atom {
            lines: vec![
              Line {
                slots: vec![
                  Slot {
                    number: 22,
                    is_drawn: false
                  },
                  Slot {
                    number: 13,
                    is_drawn: false
                  },
                  Slot {
                    number: 17,
                    is_drawn: false
                  },
                  Slot {
                    number: 11,
                    is_drawn: false
                  },
                  Slot {
                    number: 0,
                    is_drawn: false
                  }
                ]
              },
              Line {
                slots: vec![
                  Slot {
                    number: 8,
                    is_drawn: false
                  },
                  Slot {
                    number: 2,
                    is_drawn: false
                  },
                  Slot {
                    number: 23,
                    is_drawn: false
                  },
                  Slot {
                    number: 4,
                    is_drawn: false
                  },
                  Slot {
                    number: 24,
                    is_drawn: false
                  }
                ]
              },
              Line {
                slots: vec![
                  Slot {
                    number: 21,
                    is_drawn: false
                  },
                  Slot {
                    number: 9,
                    is_drawn: false
                  },
                  Slot {
                    number: 14,
                    is_drawn: false
                  },
                  Slot {
                    number: 16,
                    is_drawn: false
                  },
                  Slot {
                    number: 7,
                    is_drawn: false
                  }
                ]
              },
              Line {
                slots: vec![
                  Slot {
                    number: 6,
                    is_drawn: false
                  },
                  Slot {
                    number: 10,
                    is_drawn: false
                  },
                  Slot {
                    number: 3,
                    is_drawn: false
                  },
                  Slot {
                    number: 18,
                    is_drawn: false
                  },
                  Slot {
                    number: 5,
                    is_drawn: false
                  }
                ]
              },
              Line {
                slots: vec![
                  Slot {
                    number: 1,
                    is_drawn: false
                  },
                  Slot {
                    number: 12,
                    is_drawn: false
                  },
                  Slot {
                    number: 20,
                    is_drawn: false
                  },
                  Slot {
                    number: 15,
                    is_drawn: false
                  },
                  Slot {
                    number: 19,
                    is_drawn: false
                  }
                ]
              }
            ],
            drawn_status: HashMap::from([
              (11, false),
              (20, false),
              (22, false),
              (21, false),
              (9, false),
              (6, false),
              (8, false),
              (23, false),
              (2, false),
              (10, false),
              (13, false),
              (16, false),
              (3, false),
              (5, false),
              (15, false),
              (7, false),
              (17, false),
              (1, false),
              (4, false),
              (24, false),
              (14, false),
              (18, false),
              (0, false),
              (12, false),
              (19, false)
            ])
          },
          Atom {
            lines: vec![
              Line {
                slots: vec![
                  Slot {
                    number: 3,
                    is_drawn: false
                  },
                  Slot {
                    number: 15,
                    is_drawn: false
                  },
                  Slot {
                    number: 0,
                    is_drawn: false
                  },
                  Slot {
                    number: 2,
                    is_drawn: false
                  },
                  Slot {
                    number: 22,
                    is_drawn: false
                  }
                ]
              },
              Line {
                slots: vec![
                  Slot {
                    number: 9,
                    is_drawn: false
                  },
                  Slot {
                    number: 18,
                    is_drawn: false
                  },
                  Slot {
                    number: 13,
                    is_drawn: false
                  },
                  Slot {
                    number: 17,
                    is_drawn: false
                  },
                  Slot {
                    number: 5,
                    is_drawn: false
                  }
                ]
              },
              Line {
                slots: vec![
                  Slot {
                    number: 19,
                    is_drawn: false
                  },
                  Slot {
                    number: 8,
                    is_drawn: false
                  },
                  Slot {
                    number: 7,
                    is_drawn: false
                  },
                  Slot {
                    number: 25,
                    is_drawn: false
                  },
                  Slot {
                    number: 23,
                    is_drawn: false
                  }
                ]
              },
              Line {
                slots: vec![
                  Slot {
                    number: 20,
                    is_drawn: false
                  },
                  Slot {
                    number: 11,
                    is_drawn: false
                  },
                  Slot {
                    number: 10,
                    is_drawn: false
                  },
                  Slot {
                    number: 24,
                    is_drawn: false
                  },
                  Slot {
                    number: 4,
                    is_drawn: false
                  }
                ]
              },
              Line {
                slots: vec![
                  Slot {
                    number: 14,
                    is_drawn: false
                  },
                  Slot {
                    number: 21,
                    is_drawn: false
                  },
                  Slot {
                    number: 16,
                    is_drawn: false
                  },
                  Slot {
                    number: 12,
                    is_drawn: false
                  },
                  Slot {
                    number: 6,
                    is_drawn: false
                  }
                ]
              }
            ],
            drawn_status: HashMap::from([
              (23, false),
              (12, false),
              (8, false),
              (7, false),
              (20, false),
              (11, false),
              (4, false),
              (14, false),
              (19, false),
              (18, false),
              (22, false),
              (25, false),
              (9, false),
              (6, false),
              (3, false),
              (21, false),
              (16, false),
              (24, false),
              (10, false),
              (5, false),
              (0, false),
              (17, false),
              (13, false),
              (2, false),
              (15, false)
            ]),
          },
          Atom {
            lines: vec![
              Line {
                slots: vec![
                  Slot {
                    number: 14,
                    is_drawn: false
                  },
                  Slot {
                    number: 21,
                    is_drawn: false
                  },
                  Slot {
                    number: 17,
                    is_drawn: false
                  },
                  Slot {
                    number: 24,
                    is_drawn: false
                  },
                  Slot {
                    number: 4,
                    is_drawn: false
                  }
                ]
              },
              Line {
                slots: vec![
                  Slot {
                    number: 10,
                    is_drawn: false
                  },
                  Slot {
                    number: 16,
                    is_drawn: false
                  },
                  Slot {
                    number: 15,
                    is_drawn: false
                  },
                  Slot {
                    number: 9,
                    is_drawn: false
                  },
                  Slot {
                    number: 19,
                    is_drawn: false
                  }
                ]
              },
              Line {
                slots: vec![
                  Slot {
                    number: 18,
                    is_drawn: false
                  },
                  Slot {
                    number: 8,
                    is_drawn: false
                  },
                  Slot {
                    number: 23,
                    is_drawn: false
                  },
                  Slot {
                    number: 26,
                    is_drawn: false
                  },
                  Slot {
                    number: 20,
                    is_drawn: false
                  }
                ]
              },
              Line {
                slots: vec![
                  Slot {
                    number: 22,
                    is_drawn: false
                  },
                  Slot {
                    number: 11,
                    is_drawn: false
                  },
                  Slot {
                    number: 13,
                    is_drawn: false
                  },
                  Slot {
                    number: 6,
                    is_drawn: false
                  },
                  Slot {
                    number: 5,
                    is_drawn: false
                  }
                ]
              },
              Line {
                slots: vec![
                  Slot {
                    number: 2,
                    is_drawn: false
                  },
                  Slot {
                    number: 0,
                    is_drawn: false
                  },
                  Slot {
                    number: 12,
                    is_drawn: false
                  },
                  Slot {
                    number: 3,
                    is_drawn: false
                  },
                  Slot {
                    number: 7,
                    is_drawn: false
                  }
                ]
              }
            ],
            drawn_status: HashMap::from([
              (20, false),
              (21, false),
              (2, false),
              (15, false),
              (14, false),
              (16, false),
              (13, false),
              (17, false),
              (18, false),
              (6, false),
              (5, false),
              (24, false),
              (9, false),
              (22, false),
              (11, false),
              (3, false),
              (0, false),
              (23, false),
              (26, false),
              (10, false),
              (4, false),
              (19, false),
              (8, false),
              (12, false),
              (7, false)
            ]),
          },
        ],
      }
    );
  }
}
