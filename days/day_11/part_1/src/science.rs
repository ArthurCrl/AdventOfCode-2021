use std::collections::HashMap;
use std::fmt::Debug;

#[derive( PartialEq, Clone, Eq)]
pub struct Atom {
    pub map: HashMap<(u32, u32), u32>,
    pub length: u32,
    pub height: u32,
}

impl Debug for Atom {

    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut _res = writeln!(f,"\n<== MAP ==>").unwrap();

        for line in 0..self.height {
            for col in 0..self.length {
                if let Some(val) = self.map.get(&(line, col)) {
                   _res = write!(f, "{:?},", val).unwrap();
                }
            }
           _res = write!(f, "\n").unwrap();
        }
        return std::fmt::Result::Ok(());
    }
}

#[derive(Debug, PartialEq, Clone, Eq)]
pub struct Molecule {
    pub map: HashMap<(u32, u32), u32>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Cell {}
