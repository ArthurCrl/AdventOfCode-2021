use std::collections::HashMap;

use crate::science::Atom;
pub fn process_atoms(atom: Atom, nb_loops: u32) -> usize {
    let mut cur_atom = atom.clone();
    let mut cpt = 0;
    for _lop in 0..nb_loops {
        let mut popped_indexes = HashMap::new();
        for line_index in 0..atom.height {
            for col_index in 0..atom.length {
                (cur_atom, popped_indexes) =
                    pop(cur_atom, line_index, col_index, popped_indexes.clone());
            }
        }
        cpt = cpt + popped_indexes.len();
    }

    return cpt;
}

fn pop(
    atom: Atom,
    line: u32,
    col: u32,
    popped_indexes: HashMap<(u32, u32), bool>,
) -> (Atom, HashMap<(u32, u32), bool>) {
    let maybe_val = atom.map.get(&(line, col));
    let mut cur_atom = atom.clone();
    let mut cur_popped_index = popped_indexes.clone();
    if let Some(_is_popped) = cur_popped_index.get(&(line, col)) {
        return (cur_atom, cur_popped_index);
    }
    if let Some(cur_val) = maybe_val {
        if *cur_val == 9 {
            cur_atom.map.insert((line, col), 0);
            cur_popped_index.insert((line, col), true);
            if line >= 1 && col >= 1 {
                (cur_atom, cur_popped_index) = pop(cur_atom, line - 1, col - 1, cur_popped_index);
            }
            if col >= 1 {
                (cur_atom, cur_popped_index) = pop(cur_atom, line, col - 1, cur_popped_index);
                (cur_atom, cur_popped_index) = pop(cur_atom, line + 1, col - 1, cur_popped_index);
            }
            (cur_atom, cur_popped_index) = pop(cur_atom, line + 1, col, cur_popped_index);
            (cur_atom, cur_popped_index) = pop(cur_atom, line + 1, col + 1, cur_popped_index);
            (cur_atom, cur_popped_index) = pop(cur_atom, line, col + 1, cur_popped_index);
            if line >= 1 {
                (cur_atom, cur_popped_index) = pop(cur_atom, line - 1, col + 1, cur_popped_index);
                (cur_atom, cur_popped_index) = pop(cur_atom, line - 1, col, cur_popped_index);
            }
        } else {
            cur_atom.map.insert((line, col), *cur_val + 1);
        }
    }
    // println!(
    //     "After modification of pos {:?},{:?}: {:?} indexes: {:?}",
    //     line, col, cur_atom, popped_indexes
    // );
    return (cur_atom, cur_popped_index);
}
#[cfg(test)]
mod tests {

    use crate::parser;

    use super::*;

    #[test]
    fn validate_atom_processing_10() {
        let file_content_input: String = parser::read_file_input("./input/sample.txt");
        let input_atom = parser::parse_file_content(&file_content_input);
        assert_eq!(process_atoms(input_atom, 10), 204);
    }

     #[test]
    fn validate_atom_processing_100() {
        let file_content_input: String = parser::read_file_input("./input/sample.txt");
        let input_atom = parser::parse_file_content(&file_content_input);
        assert_eq!(process_atoms(input_atom, 100), 1656);
    }
}
