use crate::science::Atom;
use regex::Regex;
use std::collections::HashMap;
use std::fs;
use std::io::Read;
use std::path::Path;

pub fn read_file_input(path: &str) -> String {
    let mut file =
        fs::File::open(Path::new(path)).expect("An error occurred while opening the file");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("An error occurred while reading the file");
    return contents;
}

pub fn parse_file_content(data: &String) -> Atom {
    let mut parsed_data = HashMap::new();
    let mut line_index: u32 = 0;
    let mut column_index: u32 = 0;
    for line in data.lines() {
        column_index = 0;
        let digits = parse_line(line);
        for digit in digits {
            parsed_data.insert((line_index, column_index), digit);
            column_index = column_index + 1;
        }
        line_index = line_index + 1;
    }

    return Atom {
        map: parsed_data,
        height: line_index,
        length: column_index,
    };
}

fn parse_line(line: &str) -> Vec<u32> {
    let regex = Regex::new(r"\d").unwrap();
    let captures = regex.captures_iter(line);
    let mut digits = Vec::new();
    for capture in captures {
        let digit = capture
            .get(0)
            .unwrap()
            .as_str()
            .chars()
            .next()
            .unwrap()
            .to_digit(10_u32)
            .unwrap();
        digits.push(digit);
    }
    return digits;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_parse_standard_line_correctly() {
        assert_eq!(parse_line("2199943210"), vec![2, 1, 9, 9, 9, 4, 3, 2, 1, 0]);
    }

    #[test]
    fn should_parse_sample_file_correctly() {
        assert_eq!(
            parse_file_content(&read_file_input("./input/sample.txt")),
            Atom {
                length: 10,
                height: 10,
                map: HashMap::from([
                    ((0, 7), 2),
                    ((5, 6), 4),
                    ((5, 9), 5),
                    ((0, 0), 5),
                    ((3, 1), 1),
                    ((2, 4), 5),
                    ((9, 1), 2),
                    ((5, 7), 6),
                    ((6, 2), 7),
                    ((1, 8), 1),
                    ((4, 0), 6),
                    ((5, 3), 7),
                    ((6, 5), 4),
                    ((2, 5), 5),
                    ((6, 3), 6),
                    ((6, 6), 1),
                    ((0, 1), 4),
                    ((7, 4), 8),
                    ((8, 1), 8),
                    ((9, 3), 3),
                    ((1, 0), 2),
                    ((0, 3), 3),
                    ((0, 6), 3),
                    ((2, 0), 5),
                    ((2, 9), 3),
                    ((3, 9), 6),
                    ((5, 1), 1),
                    ((4, 2), 5),
                    ((0, 9), 3),
                    ((3, 2), 4),
                    ((6, 7), 7),
                    ((8, 7), 5),
                    ((9, 6), 1),
                    ((5, 2), 6),
                    ((8, 0), 4),
                    ((5, 4), 5),
                    ((7, 9), 4),
                    ((9, 7), 5),
                    ((1, 4), 8),
                    ((4, 8), 7),
                    ((4, 6), 5),
                    ((5, 5), 2),
                    ((4, 1), 3),
                    ((8, 9), 4),
                    ((9, 9), 6),
                    ((1, 1), 7),
                    ((0, 2), 8),
                    ((4, 9), 8),
                    ((6, 9), 1),
                    ((7, 0), 6),
                    ((8, 8), 5),
                    ((3, 7), 1),
                    ((6, 0), 2),
                    ((9, 0), 5),
                    ((9, 8), 2),
                    ((2, 1), 2),
                    ((7, 2), 8),
                    ((2, 3), 4),
                    ((0, 4), 1),
                    ((0, 5), 4),
                    ((4, 4), 3),
                    ((5, 0), 4),
                    ((1, 7), 7),
                    ((2, 7), 1),
                    ((1, 3), 5),
                    ((3, 4), 3),
                    ((7, 6), 1),
                    ((7, 7), 1),
                    ((8, 2), 4),
                    ((8, 3), 6),
                    ((2, 8), 7),
                    ((1, 6), 4),
                    ((1, 9), 1),
                    ((9, 5), 5),
                    ((1, 2), 4),
                    ((3, 3), 1),
                    ((4, 5), 8),
                    ((6, 4), 8),
                    ((7, 5), 8),
                    ((8, 4), 8),
                    ((3, 6), 6),
                    ((0, 8), 2),
                    ((1, 5), 5),
                    ((7, 3), 2),
                    ((4, 3), 7),
                    ((9, 2), 8),
                    ((3, 8), 4),
                    ((6, 1), 1),
                    ((7, 8), 3),
                    ((2, 2), 6),
                    ((6, 8), 2),
                    ((7, 1), 8),
                    ((8, 6), 8),
                    ((8, 5), 4),
                    ((2, 6), 6),
                    ((9, 4), 7),
                    ((5, 8), 4),
                    ((3, 5), 3),
                    ((3, 0), 6),
                    ((4, 7), 4)
                ])
            },
        )
    }
}
