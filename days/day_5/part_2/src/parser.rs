use crate::science::Atom;
use crate::science::Molecule;
use regex::Regex;
use std::fs;
use std::io::Read;
use std::path::Path;

pub fn read_file_input(path: &str) -> String {
  let mut file = fs::File::open(Path::new(path)).expect("An error occurred while opening the file");
  let mut contents = String::new();
  file
    .read_to_string(&mut contents)
    .expect("An error occurred while reading the file");
  return contents;
}

pub fn parse_file_content(data: &String) -> Vec<Molecule> {
  let mut molecules = Vec::new();
  for (_index, line) in data.lines().enumerate() {
    let molecule = parse_line(line);
    molecules.push(molecule);
  }
  return molecules;
}

fn parse_line(line: &str) -> Molecule {
  let regex = Regex::new(r"^(\d+),(\d+) -> (\d+),(\d+)$").unwrap();
  let captures = regex.captures(line).unwrap();
  let from = Atom {
    x: captures.get(1).unwrap().as_str().parse().unwrap(),
    y: captures.get(2).unwrap().as_str().parse().unwrap(),
  };
  let to = Atom {
    x: captures.get(3).unwrap().as_str().parse().unwrap(),
    y: captures.get(4).unwrap().as_str().parse().unwrap(),
  };
  let molecule = Molecule { from, to };
  return molecule;
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn should_parse_standard_line_correctly() {
    assert_eq!(
      parse_line("0,9 -> 5,9"),
      Molecule {
        from: Atom { x: 0, y: 9 },
        to: Atom { x: 5, y: 9 }
      }
    );
  }

  #[test]
  fn should_parse_sample_file_correctly() {
    assert_eq!(
      parse_file_content(&read_file_input("./input/sample.txt")),
      vec![
        Molecule {
          from: Atom { x: 0, y: 9 },
          to: Atom { x: 5, y: 9 }
        },
        Molecule {
          from: Atom { x: 8, y: 0 },
          to: Atom { x: 0, y: 8 }
        },
        Molecule {
          from: Atom { x: 9, y: 4 },
          to: Atom { x: 3, y: 4 }
        },
        Molecule {
          from: Atom { x: 2, y: 2 },
          to: Atom { x: 2, y: 1 }
        },
        Molecule {
          from: Atom { x: 7, y: 0 },
          to: Atom { x: 7, y: 4 }
        },
        Molecule {
          from: Atom { x: 6, y: 4 },
          to: Atom { x: 2, y: 0 }
        },
        Molecule {
          from: Atom { x: 0, y: 9 },
          to: Atom { x: 2, y: 9 }
        },
        Molecule {
          from: Atom { x: 3, y: 4 },
          to: Atom { x: 1, y: 4 }
        },
        Molecule {
          from: Atom { x: 0, y: 0 },
          to: Atom { x: 8, y: 8 }
        },
        Molecule {
          from: Atom { x: 5, y: 5 },
          to: Atom { x: 8, y: 2 }
        }
      ]
    )
  }
}
