#[derive(Debug, PartialEq, Clone)]
pub struct Molecule {
  pub from: Atom,
  pub to: Atom,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Atom {
  pub x: usize,
  pub y: usize,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Cell {
  pub table: Vec<Vec<usize>>,
}

pub trait Table {
  fn init_table(&mut self, max_x: usize, max_y: usize);
  fn fill_table(&mut self, molecules: Vec<Molecule>);
  fn compute_score(&self) -> usize;
}

impl Table for Cell {
  fn init_table(&mut self, max_x: usize, max_y: usize) {
    for _y in 0..max_y + 1 {
      let mut line = Vec::new();
      for _x in 0..max_x + 1 {
        line.push(0);
      }
      self.table.push(line);
    }
  }

  fn fill_table(&mut self, molecules: Vec<Molecule>) {
    for molecule in molecules {
      if molecule.from.x == molecule.to.x || molecule.from.y == molecule.to.y {
        let from_y = if molecule.from.y <= molecule.to.y {
          molecule.from.y
        } else {
          molecule.to.y
        };
        let to_y = if molecule.from.y <= molecule.to.y {
          molecule.to.y
        } else {
          molecule.from.y
        };
        let from_x = if molecule.from.x <= molecule.to.x {
          molecule.from.x
        } else {
          molecule.to.x
        };
        let to_x = if molecule.from.x <= molecule.to.x {
          molecule.to.x
        } else {
          molecule.from.x
        };
        for y in from_y..to_y + 1 {
          for x in from_x..to_x + 1 {
            self.table[y][x] = self.table[y][x] + 1;
          }
        }
      }
    }
  }

  fn compute_score(&self) -> usize {
    let mut score = 0;
    for y in 0..self.table.len() {
      let line = self.table[y].clone();
      for x in 0..line.len() {
        if line[x] > 1 {
          score = score + 1;
        }
      }
    }
    return score;
  }
}
