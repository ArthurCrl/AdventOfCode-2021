use crate::science::Cell;
use crate::science::Molecule;
use crate::science::Table;

pub fn process_molecules(molecules: Vec<Molecule>) -> usize {
  let (max_x, max_y) = compute_maxs(molecules.clone());
  let mut board = Cell { table: Vec::new() };
  board.init_table(max_x, max_y);
  board.fill_table(molecules);
  let score = board.compute_score();
  return score;
}

fn compute_maxs(molecules: Vec<Molecule>) -> (usize, usize) {
  let mut max_x = 0;
  let mut max_y = 0;
  for molecule in molecules {
    if molecule.from.x > max_x {
      max_x = molecule.from.x;
    }
    if molecule.from.y > max_y {
      max_y = molecule.from.y;
    }
    if molecule.to.x > max_x {
      max_x = molecule.to.x;
    }
    if molecule.to.y > max_y {
      max_x = molecule.to.y;
    }
  }
  return (max_x, max_y);
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::science::Atom;
  #[test]
  fn validate_atom_processing() {
    let input_data = vec![
      Molecule {
        from: Atom { x: 0, y: 9 },
        to: Atom { x: 5, y: 9 },
      },
      Molecule {
        from: Atom { x: 8, y: 0 },
        to: Atom { x: 0, y: 8 },
      },
      Molecule {
        from: Atom { x: 9, y: 4 },
        to: Atom { x: 3, y: 4 },
      },
      Molecule {
        from: Atom { x: 2, y: 2 },
        to: Atom { x: 2, y: 1 },
      },
      Molecule {
        from: Atom { x: 7, y: 0 },
        to: Atom { x: 7, y: 4 },
      },
      Molecule {
        from: Atom { x: 6, y: 4 },
        to: Atom { x: 2, y: 0 },
      },
      Molecule {
        from: Atom { x: 0, y: 9 },
        to: Atom { x: 2, y: 9 },
      },
      Molecule {
        from: Atom { x: 3, y: 4 },
        to: Atom { x: 1, y: 4 },
      },
      Molecule {
        from: Atom { x: 0, y: 0 },
        to: Atom { x: 8, y: 8 },
      },
      Molecule {
        from: Atom { x: 5, y: 5 },
        to: Atom { x: 8, y: 2 },
      },
    ];
    let expected_data = 5;
    assert_eq!(process_molecules(input_data), expected_data)
  }
}
