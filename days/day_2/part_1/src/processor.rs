use crate::science::Atom;
use crate::science::Molecule;

pub fn process_atoms(atoms: Vec<Atom>) -> Vec<Molecule> {
  let mut molecules = Vec::new();
  let mut molecule = Molecule { depth: 0, position: 0 };
  for atom in atoms {
    match atom {
      Atom::Forward(value) => molecule.position = molecule.position + value,
      Atom::Downward(value) => molecule.depth = molecule.depth + value,
      Atom::Upward(value) => molecule.depth = molecule.depth - value,
      Atom::Unknown => println!("Ignored atom")
    } 
  }
  molecules.push(molecule);
  return molecules;
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn validate_atom_processing() {
    let input_data = vec![
        Atom::Forward(5),
        Atom::Downward(5),
        Atom::Forward(8),
        Atom::Upward(3),
        Atom::Downward(8),
        Atom::Forward(2)
      ];
    let expected_data = vec![
      Molecule { depth: 10, position: 15 }
    ];
    assert_eq!(process_atoms(input_data), expected_data)
  }
}
