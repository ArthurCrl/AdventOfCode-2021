#[derive(Debug, PartialEq)]
pub enum Atom {
  Forward(u64),
  Downward(u64),
  Upward(u64),
  Unknown
}

#[derive(Debug, PartialEq)]
pub struct Molecule {
  pub depth: u64,
  pub position: u64,
}
