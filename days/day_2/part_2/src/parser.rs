use crate::science::Atom;
use regex::Regex;
use std::fs;
use std::io::Read;
use std::path::Path;

pub fn read_file_input(path: &str) -> String {
  let mut file = fs::File::open(Path::new(path)).expect("An error occurred while opening the file");
  let mut contents = String::new();
  file
    .read_to_string(&mut contents)
    .expect("An error occurred while reading the file");
  return contents;
}

pub fn parse_file_content(data: &String) -> Vec<Atom> {
  let mut parsed_data = Vec::new();
  for line in data.lines() {
    parsed_data.push(parse_line(line))
  }
  return parsed_data;
}

fn parse_line(line: &str) -> Atom {
  let regex = Regex::new(r"^(\w+) (\d+)$").unwrap();
  let captures = regex.captures(line).unwrap();
  let direction = captures.get(1).unwrap().as_str();
  let amount = captures.get(2).unwrap().as_str();
  match direction {
    "forward" => return Atom::Forward(amount.parse().unwrap()),
    "down" => return Atom::Downward(amount.parse().unwrap()),
    "up" => return Atom::Upward(amount.parse().unwrap()),
    _ =>  return Atom::Unknown

  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn should_parse_standard_line_correctly() {
    assert_eq!(parse_line("forward 5"), Atom::Forward(5));
    assert_eq!(parse_line("down 5"), Atom::Downward(5));
    assert_eq!(parse_line("up 8"), Atom::Upward(8));
  }

  #[test]
  fn should_parse_sample_file_correctly() {
    assert_eq!(
      parse_file_content(&read_file_input("./input/sample.txt")),
      vec![
        Atom::Forward(5),
        Atom::Downward(5),
        Atom::Forward(8),
        Atom::Upward(3),
        Atom::Downward(8),
        Atom::Forward(2)
      ]
    );
  }
}
