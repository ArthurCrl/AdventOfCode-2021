mod parser;
mod processor;
mod science;

fn main() {
    let file_content: String = parser::read_file_input("./input/input.txt");
    let atoms: Vec<science::Atom> = parser::parse_file_content(&file_content);
    let result: Vec<science::Molecule> = processor::process_atoms(atoms);
    println!("Results: {:?}, {}", result[0], result[0].depth * result[0].position)
}
