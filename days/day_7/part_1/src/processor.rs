use crate::science::Atom;

pub fn process_atoms(atoms: Vec<Atom>) -> isize {
  let max = atoms.iter().max().unwrap().depth;
  let min = atoms.iter().min().unwrap().depth;
  let mut fuels = Vec::new();

  for target in min..max + 1 {
    let mut fuel = 0;
    for atom in atoms.clone() {
      let delta = (target - atom.depth).abs();
      fuel = fuel + delta;
    }
    fuels.push(fuel);
  }
  let min_fuel = fuels.iter().min().unwrap();
  return *min_fuel;
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn validate_atom_processing() {
    let input_data = vec![
      Atom { depth: 16 },
      Atom { depth: 1 },
      Atom { depth: 2 },
      Atom { depth: 0 },
      Atom { depth: 4 },
      Atom { depth: 2 },
      Atom { depth: 7 },
      Atom { depth: 1 },
      Atom { depth: 2 },
      Atom { depth: 14 },
    ];
    assert_eq!(process_atoms(input_data), 37);
  }
}
