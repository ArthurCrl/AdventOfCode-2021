#[derive(Debug, PartialEq, Clone, PartialOrd, Ord, Eq)]
pub struct Atom {
  pub depth: isize,
}

#[derive(Debug, PartialEq)]
pub struct Molecule {}

#[derive(Debug, PartialEq, Clone)]
pub struct Cell {}
