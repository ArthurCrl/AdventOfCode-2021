use crate::science::Atom;
use regex::Regex;
use std::fs;
use std::io::Read;
use std::path::Path;

pub fn read_file_input(path: &str) -> String {
  let mut file = fs::File::open(Path::new(path)).expect("An error occurred while opening the file");
  let mut contents = String::new();
  file
    .read_to_string(&mut contents)
    .expect("An error occurred while reading the file");
  return contents;
}

pub fn parse_file_content(data: &String) -> Vec<Atom> {
  let mut parsed_data = Vec::new();
  for (_index, line) in data.lines().enumerate() {
    let mut parsed_atoms = parse_line(line);
    parsed_data.append(&mut parsed_atoms)
  }
  return parsed_data;
}

fn parse_line(line: &str) -> Vec<Atom> {
  let regex = Regex::new(r"(?:(\d+),?)").unwrap();
  let captures = regex.captures_iter(line);
  let mut atoms = Vec::new();
  for c in captures {
    atoms.push(Atom {
      depth: c[1].parse().unwrap(),
    })
  }
  return atoms;
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn should_parse_standard_line_correctly() {
    assert_eq!(
      parse_line("16,1,2"),
      vec![Atom { depth: 16 }, Atom { depth: 1 }, Atom { depth: 2 }]
    );
  }

  #[test]
  fn should_parse_sample_file_correctly() {
    assert_eq!(
      parse_file_content(&read_file_input("./input/sample.txt")),
      vec![
        Atom { depth: 16 },
        Atom { depth: 1 },
        Atom { depth: 2 },
        Atom { depth: 0 },
        Atom { depth: 4 },
        Atom { depth: 2 },
        Atom { depth: 7 },
        Atom { depth: 1 },
        Atom { depth: 2 },
        Atom { depth: 14 }
      ]
    );
  }
}
