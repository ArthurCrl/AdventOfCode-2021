#![feature(destructuring_assignment)]

mod parser;
mod processor;
mod science;

fn main() {
    let file_content: String = parser::read_file_input("./input/input.txt");
    let datas = parser::parse_file_content(&file_content);
    let result: usize = processor::process_atoms(datas);
    println!("Results: {:?}", result)
}
