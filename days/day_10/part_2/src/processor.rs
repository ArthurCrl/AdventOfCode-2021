use std::collections::HashMap;

pub fn process_atoms(datas: Vec<Vec<char>>) -> usize {
    let mut mappings = HashMap::new();
    mappings.insert(']', '[');
    mappings.insert(')', '(');
    mappings.insert('}', '{');
    mappings.insert('>', '<');
    let mut scores = Vec::new();
    let mut points: HashMap<char, usize> = HashMap::new();
    points.insert('(', 1);
    points.insert('[', 2);
    points.insert('{', 3);
    points.insert('<', 4);
    for tokens in datas {
        let mut stack = Vec::new();
        let mut is_corrupted = false;
        for token in tokens {
            if stack.len() == 0 {
                stack.push(token)
            } else {
                let last_token = stack[stack.len() - 1];
                if ['(', '[', '{', '<'].contains(&token) {
                    stack.push(token);
                } else {
                    if ['(', '[', '{', '<'].contains(&last_token) {
                        if &last_token == mappings.get(&token).unwrap() {
                            stack.pop();
                        } else {
                            is_corrupted = true;
                            break;
                        }
                    } else {
                        println!("uh oh");
                    }
                }
            }
        }
        if !is_corrupted {
            let mut counter = 0;
            let reverted_stack = stack.clone();
            for token in reverted_stack.iter().rev() {
                counter = 5 * counter + points.get(&token).unwrap();
            }
            println!("Score: {:?}, stack: {:?}", counter, stack);
            scores.push(counter)
        }
    }
    scores.sort();
    return scores[(scores.len() - 1) / 2];
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn validate_atom_processing() {
        let input_data = vec![
            vec![
                '[', '(', '{', '(', '<', '(', '(', ')', ')', '[', ']', '>', '[', '[', '{', '[',
                ']', '{', '<', '(', ')', '<', '>', '>',
            ],
            vec![
                '[', '(', '(', ')', '[', '<', '>', ']', ')', ']', '(', '{', '[', '<', '{', '<',
                '<', '[', ']', '>', '>', '(',
            ],
            vec![
                '{', '(', '[', '(', '<', '{', '}', '[', '<', '>', '[', ']', '}', '>', '{', '[',
                ']', '{', '[', '(', '<', '(', ')', '>',
            ],
            vec![
                '(', '(', '(', '(', '{', '<', '>', '}', '<', '{', '<', '{', '<', '>', '}', '{',
                '[', ']', '{', '[', ']', '{', '}',
            ],
            vec![
                '[', '[', '<', '[', '(', '[', ']', ')', ')', '<', '(', '[', '[', '{', '}', '[',
                '[', '(', ')', ']', ']', ']',
            ],
            vec![
                '[', '{', '[', '{', '(', '{', '}', ']', '{', '}', '}', '(', '[', '{', '[', '{',
                '{', '{', '}', '}', '(', '[', ']',
            ],
            vec![
                '{', '<', '[', '[', ']', ']', '>', '}', '<', '{', '[', '{', '[', '{', '[', ']',
                '{', '(', ')', '[', '[', '[', ']',
            ],
            vec![
                '[', '<', '(', '<', '(', '<', '(', '<', '{', '}', ')', ')', '>', '<', '(', '[',
                ']', '(', '[', ']', '(', ')',
            ],
            vec![
                '<', '{', '(', '[', '(', '[', '[', '(', '<', '>', '(', ')', ')', '{', '}', ']',
                '>', '(', '<', '<', '{', '{',
            ],
            vec![
                '<', '{', '(', '[', '{', '{', '}', '}', '[', '<', '[', '[', '[', '<', '>', '{',
                '}', ']', ']', ']', '>', '[', ']', ']',
            ],
        ];
        assert_eq!(process_atoms(input_data), 288957);
    }
}
