use std::collections::HashMap;

#[derive(Debug, PartialEq, Clone, Eq)]
pub struct Atom {
    pub map: HashMap<(u32, u32), u32>,
}

#[derive(Debug, PartialEq)]
pub struct Molecule {}

#[derive(Debug, PartialEq, Clone)]
pub struct Cell {}
