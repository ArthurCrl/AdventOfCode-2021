use std::collections::HashMap;


pub fn process_atoms(datas: Vec<Vec<char>>) -> usize {
    let mut mappings = HashMap::new();
    mappings.insert(']','[');
    mappings.insert(')','(');
    mappings.insert('}','{');
    mappings.insert('>','<');

    let mut points : HashMap<char, usize> = HashMap::new();
    points.insert(')', 3);
    points.insert(']',57);
    points.insert('}',1197);
    points.insert('>',25137);
    let mut counter = 0;
    for tokens in datas {
        let mut stack = Vec::new();
        for token in tokens {
            if stack.len() == 0 {
                stack.push(token)
            } else {
                let last_token = stack[stack.len()-1];
                 if ['(', '[', '{', '<'].contains(&token) {
                     stack.push(token);
                 } else {
                    if ['(', '[', '{', '<'].contains(&last_token) {
                        if &last_token == mappings.get(&token).unwrap() {
                            stack.pop();
                        } else {
                            counter = counter + points.get(&token).unwrap();
                            break;
                        }
                    } else {
                        println!("uh oh");
                    }
                 }
            }

        }
    }
    return counter
}


#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn validate_atom_processing() {
        let input_data = 
            vec![
                vec![
                    '[', '(', '{', '(', '<', '(', '(', ')', ')', '[', ']', '>', '[', '[', '{', '[',
                    ']', '{', '<', '(', ')', '<', '>', '>'
                ],
                vec![
                    '[', '(', '(', ')', '[', '<', '>', ']', ')', ']', '(', '{', '[', '<', '{', '<',
                    '<', '[', ']', '>', '>', '('
                ],
                vec![
                    '{', '(', '[', '(', '<', '{', '}', '[', '<', '>', '[', ']', '}', '>', '{', '[',
                    ']', '{', '[', '(', '<', '(', ')', '>'
                ],
                vec![
                    '(', '(', '(', '(', '{', '<', '>', '}', '<', '{', '<', '{', '<', '>', '}', '{',
                    '[', ']', '{', '[', ']', '{', '}'
                ],
                vec![
                    '[', '[', '<', '[', '(', '[', ']', ')', ')', '<', '(', '[', '[', '{', '}', '[',
                    '[', '(', ')', ']', ']', ']'
                ],
                vec![
                    '[', '{', '[', '{', '(', '{', '}', ']', '{', '}', '}', '(', '[', '{', '[', '{',
                    '{', '{', '}', '}', '(', '[', ']'
                ],
                vec![
                    '{', '<', '[', '[', ']', ']', '>', '}', '<', '{', '[', '{', '[', '{', '[', ']',
                    '{', '(', ')', '[', '[', '[', ']'
                ],
                vec![
                    '[', '<', '(', '<', '(', '<', '(', '<', '{', '}', ')', ')', '>', '<', '(', '[',
                    ']', '(', '[', ']', '(', ')'
                ],
                vec![
                    '<', '{', '(', '[', '(', '[', '[', '(', '<', '>', '(', ')', ')', '{', '}', ']',
                    '>', '(', '<', '<', '{', '{'
                ],
                vec![
                    '<', '{', '(', '[', '{', '{', '}', '}', '[', '<', '[', '[', '[', '<', '>', '{',
                    '}', ']', ']', ']', '>', '[', ']', ']'
                ]
            ];
        assert_eq!(process_atoms(input_data), 26397);
    }
}
