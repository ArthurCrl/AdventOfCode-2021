use std::fs;
use std::io::Read;
use std::path::Path;

pub fn read_file_input(path: &str) -> String {
    let mut file =
        fs::File::open(Path::new(path)).expect("An error occurred while opening the file");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("An error occurred while reading the file");
    return contents;
}

pub fn parse_file_content(data: &String) -> Vec<Vec<char>> {
    let mut parsed_data = Vec::new();

    for line in data.lines() {
        let tokens = parse_line(line);
        parsed_data.push(tokens)
    }

    return parsed_data;
}

fn parse_line(line: &str) -> Vec<char> {
    return line.chars().collect();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_parse_standard_line_correctly() {
        assert_eq!(
            parse_line("[({(<(())[]>[[{[]{<()<>>"),
            vec![
                '[', '(', '{', '(', '<', '(', '(', ')', ')', '[', ']', '>', '[', '[', '{', '[',
                ']', '{', '<', '(', ')', '<', '>', '>'
            ]
        );
    }

    #[test]
    fn should_parse_sample_file_correctly() {
        assert_eq!(
            parse_file_content(&read_file_input("./input/sample.txt")),
            vec![
                vec![
                    '[', '(', '{', '(', '<', '(', '(', ')', ')', '[', ']', '>', '[', '[', '{', '[',
                    ']', '{', '<', '(', ')', '<', '>', '>'
                ],
                vec![
                    '[', '(', '(', ')', '[', '<', '>', ']', ')', ']', '(', '{', '[', '<', '{', '<',
                    '<', '[', ']', '>', '>', '('
                ],
                vec![
                    '{', '(', '[', '(', '<', '{', '}', '[', '<', '>', '[', ']', '}', '>', '{', '[',
                    ']', '{', '[', '(', '<', '(', ')', '>'
                ],
                vec![
                    '(', '(', '(', '(', '{', '<', '>', '}', '<', '{', '<', '{', '<', '>', '}', '{',
                    '[', ']', '{', '[', ']', '{', '}'
                ],
                vec![
                    '[', '[', '<', '[', '(', '[', ']', ')', ')', '<', '(', '[', '[', '{', '}', '[',
                    '[', '(', ')', ']', ']', ']'
                ],
                vec![
                    '[', '{', '[', '{', '(', '{', '}', ']', '{', '}', '}', '(', '[', '{', '[', '{',
                    '{', '{', '}', '}', '(', '[', ']'
                ],
                vec![
                    '{', '<', '[', '[', ']', ']', '>', '}', '<', '{', '[', '{', '[', '{', '[', ']',
                    '{', '(', ')', '[', '[', '[', ']'
                ],
                vec![
                    '[', '<', '(', '<', '(', '<', '(', '<', '{', '}', ')', ')', '>', '<', '(', '[',
                    ']', '(', '[', ']', '(', ')'
                ],
                vec![
                    '<', '{', '(', '[', '(', '[', '[', '(', '<', '>', '(', ')', ')', '{', '}', ']',
                    '>', '(', '<', '<', '{', '{'
                ],
                vec![
                    '<', '{', '(', '[', '{', '{', '}', '}', '[', '<', '[', '[', '[', '<', '>', '{',
                    '}', ']', ']', ']', '>', '[', ']', ']'
                ]
            ]
        )
    }
}
