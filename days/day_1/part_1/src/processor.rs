use crate::science::Atom;
use crate::science::Molecule;

pub fn process_atoms(atoms: Vec<Atom>) -> Vec<Molecule> {
  let mut molecules = Vec::new();
  let mut previous_atom = &Atom { value: 0 };
  for (index, atom) in atoms.iter().enumerate() {
    if previous_atom.value < atom.value && index > 0 {
      molecules.push(Molecule { index: index });
    }
    previous_atom = atom;
  }
  return molecules;
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn validate_atom_processing() {
    let input_data = vec![
      Atom { value: 199 },
      Atom { value: 200 },
      Atom { value: 208 },
      Atom { value: 210 },
      Atom { value: 200 },
      Atom { value: 207 },
      Atom { value: 240 },
      Atom { value: 269 },
      Atom { value: 260 },
      Atom { value: 263 },
    ];
    let expected_data = vec![
      Molecule { index: 1 },
      Molecule { index: 2 },
      Molecule { index: 3 },
      Molecule { index: 5 },
      Molecule { index: 6 },
      Molecule { index: 7 },
      Molecule { index: 9 },
    ];
    assert_eq!(process_atoms(input_data), expected_data)
  }
}
