use crate::science::Atom;
use std::fs;
use std::io::Read;
use std::path::Path;

pub fn read_file_input(path: &str) -> String {
  let mut file = fs::File::open(Path::new(path)).expect("An error occurred while opening the file");
  let mut contents = String::new();
  file
    .read_to_string(&mut contents)
    .expect("An error occurred while reading the file");
  return contents;
}

pub fn parse_file_content(data: &String) -> Vec<Atom> {
  let mut parsed_data = Vec::new();
  for line in data.lines() {
    parsed_data.push(parse_line(line))
  }
  return parsed_data;
}

fn parse_line(line: &str) -> Atom {
  return Atom {
    value: line.parse().unwrap(),
  };
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn should_parse_standard_line_correctly() {
    assert_eq!(parse_line("100"), Atom { value: 100 });
    assert_eq!(parse_line("0"), Atom { value: 0 });
    assert_eq!(parse_line("200"), Atom { value: 200 });
  }

  #[test]
  fn should_parse_sample_file_correctly() {
    assert_eq!(
      parse_file_content(&read_file_input("./input/sample.txt")),
      vec![
        Atom { value: 199 },
        Atom { value: 200 },
        Atom { value: 208 },
        Atom { value: 210 },
        Atom { value: 200 },
        Atom { value: 207 },
        Atom { value: 240 },
        Atom { value: 269 },
        Atom { value: 260 },
        Atom { value: 263 },
      ]
    );
  }
}
