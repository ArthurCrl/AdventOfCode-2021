#[derive(Debug, PartialEq)]
pub struct Atom {
  pub value: u64,
}

#[derive(Debug, PartialEq)]
pub struct Molecule {
  pub index: usize,
}
