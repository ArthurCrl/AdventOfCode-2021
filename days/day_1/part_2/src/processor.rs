use crate::science::Atom;
use crate::science::Molecule;

pub fn process_atoms(atoms: Vec<Atom>) -> Vec<Molecule> {
  let mut molecules = Vec::new();
  let mut final_molecules = Vec::new();

  let mut previous_molecules = &Molecule { value: 0 };
  for index in 0..atoms.len() {
    if index < (atoms.len() - 2) {
      molecules.push(Molecule {
        value: atoms[index].value + atoms[index + 1].value + atoms[index + 2].value,
      })
    }
  }
  for (index, molecule) in molecules.iter().enumerate() {
    if previous_molecules.value < molecule.value && index > 0 {
      final_molecules.push(*molecule);
    }
    previous_molecules = molecule
  }
  return final_molecules;
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn validate_atom_processing() {
    let input_data = vec![
      Atom { value: 199 },
      Atom { value: 200 },
      Atom { value: 208 },
      Atom { value: 210 },
      Atom { value: 200 },
      Atom { value: 207 },
      Atom { value: 240 },
      Atom { value: 269 },
      Atom { value: 260 },
      Atom { value: 263 },
    ];
    let expected_data = vec![
      Molecule { value: 618 },
      Molecule { value: 647 },
      Molecule { value: 716 },
      Molecule { value: 769 },
      Molecule { value: 792 },
    ];
    assert_eq!(process_atoms(input_data), expected_data)
  }
}
