#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Atom {
  pub value: u64,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Molecule {
  pub value: u64,
}
