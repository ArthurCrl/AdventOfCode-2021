use std::collections::HashMap;

use crate::science::Atom;
pub fn process_atoms(atom: Atom) -> usize {
    let mut current_atom = atom.clone();
    let mut bassins = Vec::new();
    while current_atom.map.len() > 0 {
        let bassin = HashMap::new();
        let (line, col) = current_atom.map.keys().into_iter().next().unwrap();
        let (updated_atom, bassin): (Atom, HashMap<(u32, u32), bool>) =
            explore_bassin(*line, *col, current_atom, bassin);

        // println!("bassin: {:?}", bassin);
        if bassin.len() > 0 {
            bassins.push(bassin);
        }
        current_atom = updated_atom;
    }

    let mut sizes: Vec<usize> = bassins.iter().map(|b| b.len()).collect();
    sizes.sort();
    let length = sizes.len();
    return sizes[length - 1] * sizes[length - 2] * sizes[length - 3];
}

fn explore_bassin(
    line: u32,
    col: u32,
    atom: Atom,
    bassin: HashMap<(u32, u32), bool>,
) -> (Atom, HashMap<(u32, u32), bool>) {
    let loc = atom.map.get(&(line, col));
    let mut new_atom = atom.clone();
    let mut new_basin = bassin.clone();
    let already_treated = new_basin.get(&(line, col));
    if let None = already_treated {
        if let Some(height) = loc {
            if *height != 9 {
                // println!("*: {:?},{:?}", line, col);
                new_basin.insert((line, col), true);
                new_atom.map.remove(&(line, col));
                (new_atom, new_basin) =
                    explore_bassin(line + 1, col, new_atom.clone(), new_basin.clone());
                if line > 0 {
                    (new_atom, new_basin) =
                        explore_bassin(line - 1, col, new_atom.clone(), new_basin.clone());
                }
                (new_atom, new_basin) =
                    explore_bassin(line, col + 1, new_atom.clone(), new_basin.clone());
                if col > 0 {
                    (new_atom, new_basin) =
                        explore_bassin(line, col - 1, new_atom.clone(), new_basin.clone());
                }
            } else {
                new_atom.map.remove(&(line, col));
            }
        }
    }

    return (new_atom, new_basin);
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use super::*;

    #[test]
    fn validate_atom_processing() {
        let input_data = Atom {
            map: HashMap::from([
                ((1, 9), 1),
                ((1, 0), 3),
                ((0, 6), 3),
                ((1, 1), 9),
                ((2, 1), 8),
                ((3, 5), 9),
                ((0, 4), 9),
                ((0, 8), 1),
                ((3, 7), 7),
                ((3, 9), 9),
                ((0, 5), 4),
                ((4, 3), 9),
                ((1, 3), 7),
                ((1, 7), 9),
                ((0, 7), 2),
                ((2, 6), 9),
                ((3, 6), 6),
                ((4, 4), 9),
                ((2, 9), 2),
                ((0, 3), 9),
                ((1, 4), 8),
                ((2, 0), 9),
                ((3, 0), 8),
                ((3, 8), 8),
                ((4, 5), 6),
                ((0, 1), 1),
                ((4, 6), 5),
                ((1, 2), 8),
                ((4, 8), 7),
                ((4, 9), 8),
                ((1, 8), 2),
                ((2, 3), 6),
                ((3, 2), 6),
                ((4, 7), 6),
                ((2, 8), 9),
                ((2, 7), 8),
                ((2, 4), 7),
                ((4, 2), 9),
                ((3, 4), 8),
                ((4, 1), 8),
                ((4, 0), 9),
                ((0, 2), 9),
                ((2, 5), 8),
                ((3, 3), 7),
                ((0, 9), 0),
                ((1, 5), 9),
                ((2, 2), 5),
                ((3, 1), 7),
                ((1, 6), 4),
                ((0, 0), 2),
            ]),
        };
        assert_eq!(process_atoms(input_data), 1134);
    }
}
