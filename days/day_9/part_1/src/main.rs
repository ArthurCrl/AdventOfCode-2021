
mod parser;
mod processor;
mod science;

fn main() {
    let file_content: String = parser::read_file_input("./input/input.txt");
    let (atom, nb_lines, nb_cols) = parser::parse_file_content(&file_content);
    let result: u32 = processor::process_atoms(atom, nb_lines, nb_cols);
    println!("Results: {:?}", result)
}
