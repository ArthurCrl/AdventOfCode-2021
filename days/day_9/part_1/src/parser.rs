use crate::science::Atom;
use regex::Regex;
use std::collections::HashMap;
use std::fs;
use std::io::Read;
use std::path::Path;

pub fn read_file_input(path: &str) -> String {
    let mut file =
        fs::File::open(Path::new(path)).expect("An error occurred while opening the file");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("An error occurred while reading the file");
    return contents;
}

pub fn parse_file_content(data: &String) -> (Atom, u32, u32) {
    let mut parsed_data = HashMap::new();
    let mut line_index: u32 = 0;
    let mut column_index: u32 = 0;
    for line in data.lines() {
        column_index = 0;
        let mut digits = parse_line(line);
        for  digit in digits {
            parsed_data.insert((line_index, column_index), digit);
            column_index = column_index + 1;
        }
        line_index = line_index + 1;
    }

    return (Atom { map: parsed_data }, line_index, column_index);
}

fn parse_line(line: &str) -> Vec<u32> {
    let regex = Regex::new(r"\d").unwrap();
    let captures = regex.captures_iter(line);
    let mut digits = Vec::new();
    for capture in captures {
        let digit = capture
            .get(0)
            .unwrap()
            .as_str()
            .chars()
            .next()
            .unwrap()
            .to_digit(10_u32)
            .unwrap();
        digits.push(digit);
    }
    return digits;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_parse_standard_line_correctly() {
        assert_eq!(parse_line("2199943210"), vec![2, 1, 9, 9, 9, 4, 3, 2, 1, 0]);
    }

    #[test]
    fn should_parse_sample_file_correctly() {
        assert_eq!(
            parse_file_content(&read_file_input("./input/sample.txt")),
            (Atom {
                map: HashMap::from([
                    ((1, 9), 1),
                    ((1, 0), 3),
                    ((0, 6), 3),
                    ((1, 1), 9),
                    ((2, 1), 8),
                    ((3, 5), 9),
                    ((0, 4), 9),
                    ((0, 8), 1),
                    ((3, 7), 7),
                    ((3, 9), 9),
                    ((0, 5), 4),
                    ((4, 3), 9),
                    ((1, 3), 7),
                    ((1, 7), 9),
                    ((0, 7), 2),
                    ((2, 6), 9),
                    ((3, 6), 6),
                    ((4, 4), 9),
                    ((2, 9), 2),
                    ((0, 3), 9),
                    ((1, 4), 8),
                    ((2, 0), 9),
                    ((3, 0), 8),
                    ((3, 8), 8),
                    ((4, 5), 6),
                    ((0, 1), 1),
                    ((4, 6), 5),
                    ((1, 2), 8),
                    ((4, 8), 7),
                    ((4, 9), 8),
                    ((1, 8), 2),
                    ((2, 3), 6),
                    ((3, 2), 6),
                    ((4, 7), 6),
                    ((2, 8), 9),
                    ((2, 7), 8),
                    ((2, 4), 7),
                    ((4, 2), 9),
                    ((3, 4), 8),
                    ((4, 1), 8),
                    ((4, 0), 9),
                    ((0, 2), 9),
                    ((2, 5), 8),
                    ((3, 3), 7),
                    ((0, 9), 0),
                    ((1, 5), 9),
                    ((2, 2), 5),
                    ((3, 1), 7),
                    ((1, 6), 4),
                    ((0, 0), 2)
                ])
            }, 10, 10)
        )
    }
}
